<?php get_header(); ?>

<!-- banner -->
<section class="blog-banner link-page-banner bg-image">
	<h2 class="banner-heading">Our Blog</h2>
	<ul class="breadcrumb">
		<li><a href="<?php echo get_site_url(); ?>" class="sm">Home</a></li>
		
		<li class="active sm">Blog</li>
	</ul>
</section>
<!-- end banner -->

  <?php
  $temp = $wp_query; $wp_query= null;
  $wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged); ?>
<section class="blog-page">
	<div class="container">
		<div class="col-md-8 col-xs-12 blog-container padding-left0">
			<!-- bolg-1 -->
			<?php  while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
			<div class="col-xs-12 blog-page-div no-padding">
				<div class="row blog-div-row1">
					<div class="col-md-9 col-xs-12">
						<p class="blog-page-subheading"><?php the_title(); ?></p>
					</div>
					<div class="col-md-3">
						<p class="blog-page-date lg1"><?php $my_date = the_date('M j, Y', '<span>', '</span>', FALSE); echo $my_date; ?></p>
					</div>
				</div>
				<div class="row">
					<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>" alt="blog-image" />
				</div>
				<div class="row blog-div-row3">
					<?php the_excerpt(); ?>
					 <a href="<?php the_permalink(); ?>" class="btn border-btn2 blog-page-reading-btn">Continue Reading</a>
				</div>
			</div>
			<?php endwhile; ?>

			<div class="col-xs-12 no-padding blog-pagination">
				<nav aria-label="...">
 	 				<ul class="pagination">
 	 				<?php if ($paged > 1) { ?>
					    <!-- <li class="page-item active">
					    	<a class="page-link" href="JavaScript:Void(0);">1</a>
					    </li>
					    <li class="page-item">
					      	<a class="page-link" href="JavaScript:Void(0);">2 <span class="sr-only">(current)</span></a>
					    </li>
					    <li class="page-item">
					    	<a class="page-link" href="JavaScript:Void(0);">3</a>
					    </li> -->
					    <li class="page-item"><?php previous_posts_link('Previous'); ?></li>
					    <li class="page-item"><?php next_posts_link('NEXT'); ?></li>
                          <?php } else { ?>
					    <li class="page-item"><?php next_posts_link('NEXT'); ?></li>
					    <?php } ?>
  					</ul>
				</nav>
			</div>
		</div> 
		<div class="col-md-4 col-sm-6 col-xs-12 blog-aside">
			<div class="blog-aside-div blog-page-div clearfix">
				<p class="lg1">Recent Posts</p>
				<?php
                        $args = array( 'numberposts' => 2, 'order'=> 'ASC', 'orderby' => 'title' );
                        $postslist = get_posts( $args );
                        foreach ($postslist as $post) :  setup_postdata($post); ?>
				<div class="blog-link">
					<div class="col-xs-1 no-padding"><span class="wkl-paper"></span></div>
					<div class="col-xs-11 padding-left0"><p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p></div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 blog-aside">
			<div class="blog-aside-div blog-page-div clearfix">
				<p class="lg1">Popular Posts</p>
				
					<div class="col-xs-12 pop-post padding-left0">
						<a href="<?php the_permalink(); ?>"><?php echo do_shortcode('[tptn_list]'); ?></a></div>
			</div> <!-- end blog-aside-div -->
	</div>
</section>

<?php get_footer(); ?>