<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">OOP's Concept</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>OOP's Concept Training In Nagpur</h1>
				<p>If you are thinking to become a successful Web Designer and Developer. So first you must join HTML and CSS Training. This two course is the base of Web Technology. In this course, you will get to know to make web pages and how to give graphics to your website. It is very useful and Job Oriented IT Course.</p>
				<p>Webgurukul is a Leading IT Training Institute in Nagpur. We also provide our IT training in Wardha. We offer job Oriented Advanced IT Courses. After teaching in Webgurukul you will get 100% JOB in IT Sector. Because once you learn in webgurukul you will confident in your selected course and you will get Fast job in India or in Nagpur IT Park.</p>
				<p>Webgurukul IT Training Institute trained 3000+ Students in Nagpur and wardha. All our students placed in Reputed IT Companies and In Organization all over India. We have an expert faculty for HTML 5 and CSS3 Training.In Nagpur, there are So many coaching they doing business only for money. But webgurukul Don't, We will train you like Expert, After joining this Course what you will See?, You will see you become a professional in HTML 5 and CSS3 Training. JOIN Webgurukul TODAY.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<p class="course-details-headings">Who can Apply?</p>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<p class="course-details-headings">Required Knowledge</p>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of Photoshop else alright if don’t know.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<p class="course-details-headings">OOP's Concept Modules</p>
				<p>CSS is a languages that can use to build Style in website. In these courses, you’ll learn the all basics of CSS, build your first beutiful website, and then review some of the current CSS3 best practices. Be ready with all concepts of CSS and CSS3.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Graduating to C++ (Beginning)<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Oops </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function Prototypes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Comments</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Typecasting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Void Pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The :: operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The Const Qualifier</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Reference variables</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Functions<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function Prototypes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function Overloading</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Default Arguments in Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Call by value, address &amp; reference </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Return by value, by address &amp; By reference</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Inline Functions</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Classes in C++<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Member function</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function Definition Outside The Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes and Constructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Destructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Copy Constructor</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The this Pointer</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New and delete Operators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using new and delete</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Malloc ( ) / free ( ) versus new/delete</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes, Objects and Memory</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Structures vs. Classes</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Miscellaneous Class Issues<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Static Class Data</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Static Member Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Conversion</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Friend functions &amp; friend Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Conversion between Objects of Different Classes</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Overloading operators<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading ++, --, +, -, *, /,<,> …. &amp; Logical operators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Overloading operators between different objects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Overloading << and >> (stream operators)</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Inheritance<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Constructors in Inheritance</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Private Inheritance</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Protected Inheritance</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Functions That Are Not inherited</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Pure virtual functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Virtual Functions in Derived Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Virtual Functions and Constructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Destructors and virtual Destructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Virtual Base Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Abstract class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Abstract base class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Advanced Features<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes Within Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Friend Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Friend classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading << and >>.</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Input / Output In C++ (File operations)<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Manipulators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>File I/O with Streams</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Opening and closing files</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating database with file Operation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Binary I/O</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Elementary Database Management</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Interacting with<br>
 							  -Text files (ex: .txt, .c, .cpp) / Non-text files </span></a></li>
 							 <li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating database with file operation </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: New Advanced Features<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Class templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exception handling</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Namespaces</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>RTTI (Runtime type information)</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>STL (Standard Template library)</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Dynamic cast operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Typeid operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Typeinfo class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Data Structures with C++<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Sorting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Recursion</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Single linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Double linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Circular linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Traversing of linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Stacks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Queues</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding 2 lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Inserting a node in required position</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Deleting a node from required position</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<p class="course-details-headings">What will you are going to Learn?</p>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
			</div>

			<!-- <div class="course-details-review course-details-block col-xs-12">
				<p class="course-details-headings">Course Reviews</p>
				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<p class="text-center"> 
					<a href="JavaScript:Void(0);" class="btn border-btn1 courses-border-btn1">See More</a>
				</p>
			</div> -->
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-Oopsconcept course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div>
                <P 	class="xlg">OOP's Concept</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-Oopsconcept course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
            </div>
            <P 	class="xlg">OOP's Concept</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<?php get_footer(); ?>