<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Angular JS</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Angular JS Training In Nagpur</h1>
				<p>Angular Js is javascript Framework  to develop a Single Page Applications (SPA) this single page is in index.html. With the help of Agular js we can make  Dynamic, Rich and Fast. In Angular Js you will get Data Binding and Dependency Injection , Full Testing and Environment in this there is no plug ins , Deep Linking For Dynamic Pages , Directives, MVVM To the Rescue(Models View Model Objects), Angular also Provide Stateless Controllers. This is use for making a such a good Web Applications.</p>
				<p>Webgurukul is a first IT Training Institute In Nagpur providing Angular Js Training.  We trained near 1000+ Engineers in Angular JS , Now they are placed in Reputed IT Company In India and Nagpur. Wegurukul is a leading IT Training Center in Nagpur 3000+ Students Trained we offers All Advanced IT Courses in Nagpur and Wardha.</p>
				<p>Angular JS Training is Very important All IT Companies have a Huge Demand for Angular JS, All IT Companies offering Good Packages for fresher’s and Professionals . Don’t waste your time and join Angular JS Training in Webgurukul.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<p class="course-details-headings">Who can Apply?</p>
				<ul>
					<li><span>Undergraduate students from computer background-BE(IT), BE (Comp), BCA, BSc (Comp), BSc (IT), BCS</span></li>
					<li><span>IT Professional having knowledge of HTML, CSS, JavaScript ,jQuery and any server side technology.</span></li>
					<li><span>Post graduate students – MCA, MCM, MSc (IT), MSc (Comp. Sci.) etc</span></li>
				</ul>

				<p class="course-details-headings">Required Knowledge</p>
				<ul>
					<li><span>HTML, CSS</span></li>
					<li><span>Knowledge of Web programming language like JavaScript and jQuery</span></li>
					<li><span>Having knowledge of any Server side technology will be an added advantage</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<p class="course-details-headings">Angular JS Modules</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: AngularJS Introduction<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction To Client Side Scripting Languages</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basics of Javascript and jQuery</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to AngularJS and Its History</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why should we use AngularJS?</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Controllers<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Properties, Methods</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Binding controllers with views</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Controller hierarchy</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Sharing data between controllers</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Bootstrapping Angular APPS<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Auto bootstrap</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Custom bootstrap</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: CSS3 Backgrounds<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Data Binding<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Binding Model Objects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Model Objects Visibility</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>$scope</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>$rootScope</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Difference between $scope & $rootScope</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using $emit & $broadcast</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JSON advantages</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using JSON in Angularjs</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Use of $watch, $digest & $apply</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Understanding AngularJS Architecture</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Dependency Injection<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Dependency Injection?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Implicit DI</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Inline Array Annotated DI</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>$inject Array Annotated DI</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Expressions<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>AngularJs Expressions, AngularJS Numbers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>AngularJS Strings, AngularJS Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>AngularJS Objects</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Directives<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Power of directives</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with built in directives</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-app, ng-init</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-model, ng-repeat</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-class, ng-templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-include</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with custom directives</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Filters<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding Filters to Expressions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding Filters to Directives</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with built in filters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating custom filters</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: Events<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-click</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Hiding HTML Elements</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-disabled</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ng-show, ng-hide</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: Modules<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Controllers Pollute the Global Namespace</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>AngularJS Application Files</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 13: AngularJS XMLHttpRequest (AJAX)<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>AngularJS $http</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 14: AngularJS Forms & Input Validation</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 15: AngularJS Service Types<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Constants & Values, Factories</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Services, Providers</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 16: Single Page Applications<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is SPA?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to work with SPA in angular</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with routes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Static & dynamic routing</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 17: REST API Calls<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overview of REST API</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Use of angular resource module</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 18: Animations<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overview of transitions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Use of angular animate module</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 19: Angular with UI Frameworks<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ui boostrap</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>anguar-meterial</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 20: Behaviour Driven Development<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overview of Nodejs</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>installation of karma & jasmin</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>working with karma & jasmine</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<p class="course-details-headings">What will you are going to Learn?</p>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="course-details-review course-details-block col-xs-12">
				<p class="course-details-headings">Course Reviews</p>
				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<p class="text-center"> 
					<a href="JavaScript:Void(0);" class="btn border-btn1 courses-border-btn1">See More</a>
				</p>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-angularjs course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div>
                <P 	class="xlg">Angular JS</P>
                <p class="text-center">
					<a href="registration-page.php" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-angularjs course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
            </div>
            <P 	class="xlg">Angular JS</P>
            <p class="text-center">
				<a href="registration-page.php" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>