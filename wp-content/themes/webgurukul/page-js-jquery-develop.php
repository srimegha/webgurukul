<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">JavaScript & jQuery</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>JavaScript &amp; jQuery Training In Nagpur</h1>
				<p>If you are thinking to become a successful Web Designer and Developer. So first you must join HTML and CSS Training. This two course is the base of Web Technology. In this course, you will get to know to make web pages and how to give graphics to your website. It is very useful and Job Oriented IT Course.</p>
				<p>Webgurukul is a Leading IT Training Institute in Nagpur. We also provide our IT training in Wardha. We offer job Oriented Advanced IT Courses. After teaching in Webgurukul you will get 100% JOB in IT Sector. Because once you learn in webgurukul you will confident in your selected course and you will get Fast job in India or in Nagpur IT Park.</p>
				<p>Webgurukul IT Training Institute trained 3000+ Students in Nagpur and wardha. All our students placed in Reputed IT Companies and In Organization all over India. We have an expert faculty for HTML 5 and CSS3 Training.In Nagpur, there are So many coaching they doing business only for money. But webgurukul Don't, We will train you like Expert, After joining this Course what you will See?, You will see you become a professional in HTML 5 and CSS3 Training. JOIN Webgurukul TODAY.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of Photoshop else alright if don’t know.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">JavaScript Modules</h3>
				<p>Javascript is the language that modern developers need to know, and know well. Truly knowing Javascript will get you a job, and enable you to build quality web and server applications.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to JS</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: String Method</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: String to Array</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Math Object</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Javascript Validation</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Javascript Output</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Extracting String Characters</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Variables to Numbers</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Javascript Sliders</a>
					</li>					
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">jQuery Modules</h3>
				<p>jQuery is the most used library in the world to build Web-Pages and applications. From transitions to just simplify you code just like jquery says, write less do more.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: jQuery CSS </a>
						
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: jQuery Effect </a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: jQuery Selector </a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: jQuery DOM Manipulation </a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: jQuery JSON </a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: jQuery Validation
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: jQuery Event Handling </a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: jQuery Slider</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: jQuery AJAX</a>
					</li>					
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Master JavaScript for Web projects</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Learn the use of jQuery</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use JavaScript and jQuery in professional projects</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Design web projects more effectively and elegantly</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-JSnJquery course-icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></span>
                </div>
                <P 	class="xlg">JavaScript & jQuery</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/Web-Development.pdf" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-JSnJquery course-icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></span>
            </div>
            <P 	class="xlg">JavaScript & jQuery</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/Web-Development.pdf" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
<?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>