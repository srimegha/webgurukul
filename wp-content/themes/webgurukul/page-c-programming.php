<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">C Programming</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>C Programming Training In Nagpur</h1>
				<p>C language is the basic Programming Language, C is the first step of all latest technologies. If you want to become a good Software Engineer you have to learn the C Programming  Language. It's very easy to learn and general purpose programming Language. This language is very helpful to all IT Engineers. Webgurukul has excellent teaching staff for Programming Language joins C and C++ Batch, Webgurukul Best IT Training Center in Nagpur.</p>
				<p>C++ is advanced of C language, Once you will be trained in C language our. Webgurukul team will make you expert in C++ also. We provide you best teaching Material with notes and PDF, Videos.</p>
				<p>Webgurukul is a leading IT Training Institute in Nagpur, We offer all IT Courses for Graduate and Undergraduate Students, We trained all stream Students in Nagpur. we have another branch in Wardha also. We trained up to 3000+ Engineers in Nagpur and Wardha, 100+ Workshops. Join C, C++ Programming Language Batch and start your Software Engineering Carrer Join Webgurukul.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>Engineering Students</span></li>
					<li><span>Bachelor Students</span></li>
					<li><span>Graduate and Undergraduate Students</span></li>
					<li><span>Job Seeker</span></li>
					<li><span>Candidate those who are Interesting in Software Development</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">C Programming Modules</h3>
				<p>C is one of the most important of all programming languages. It is used to program desktop applications, compilers, tools and utilities and even hardware devices. The C language is fast and efficient.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to "C" Language<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data types</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Indentation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The "while" Loop</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The "for" Loop</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Nested Loop Statements</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The "break" &amp; "continue" Statement</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Bitwise operators</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Functions<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Need for Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Standard &amp; User defined Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to create our own header file</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Difference between "source" file &amp; "exe" file</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to create "exe" file</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Storage Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pass by Value, pass by Reference</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Return by value &amp; return by address</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Recursive Functions</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Pointers<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Memory concept</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pointers to variable</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pointers to pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pointers with operation</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Arrays
						<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Need for Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Arrays with pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Arrays with functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Types of Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>One Dimensional Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Two Dimensional Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Multi Dimensional Arrays</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Strings<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Char pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Char array (String)</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Declaring &amp; Initializing String Variables</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Strings with pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Strings with functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creation of our own "string.h"</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Array of Strings</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>"#define" (preprocessing work)</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Structures, Unions, Enum and typedef<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Enum and typedef</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Structures</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Defining Structures</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Struct with pointers , arrays, strings and functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Array ,strings as Structures Members</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Containership</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Union, Different between Structure & Union</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Graphics Programming<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>In text Mode</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>In Graphics Mode</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: File Operations <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to files</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Types of Files</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>File Pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Opening & Closing Files</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Modifying  & deleting Files</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating database with file operation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Command Line Arguments</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Dynamic memory allocation (DS)<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Purpose of dynamic memory allocation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Malloc , calloc ,realloc and free</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Sorting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Recursion</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Single linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Double linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Circular linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Stacks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Queues</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding 2 lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Inserting a node in required position</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Deleting a node from required position</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand the fundamentals of the C Programming Language</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Make yourself more marketable for entry level programming positions</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create your first C Application</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Learn one of the most popular, widly used languages in the world</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand variables and the different data types</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Apply for real-time programming positions</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand the core language that most modern languages are based on</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Learn how to write high-quality code</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-cprogramming course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                </div>
                <P 	class="xlg">C Programming</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/programming2.pdf" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-cprogramming course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            </div>
            <P 	class="xlg">C Programming</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/programming2.pdf" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>