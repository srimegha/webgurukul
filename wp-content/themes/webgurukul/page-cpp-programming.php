<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">C++ Programming</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>C++ Programming Training In Nagpur</h1>
				<p>C language is the basic Programming Language, C is the first step of all latest technologies. If you want to become a good Software Engineer you have to learn the C Programming  Language. It's very easy to learn and general purpose programming Language. This language is very helpful to all IT Engineers. Webgurukul has excellent teaching staff for Programming Language joins C and C++ Batch, Webgurukul Best IT Training Center in Nagpur.</p>
				<p>C++ is advanced of C language, Once you will be trained in C language our. Webgurukul team will make you expert in C++ also. We provide you best teaching Material with notes and PDF, Videos.</p>
				<p>Webgurukul is a leading IT Training Institute in Nagpur, We offer all IT Courses for Graduate and Undergraduate Students, We trained all stream Students in Nagpur. we have another branch in Wardha also. We trained up to 3000+ Engineers in Nagpur and Wardha, 100+ Workshops. Join C, C++ Programming Language Batch and start your Software Engineering Carrer Join Webgurukul.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>Basic knowledge of C programming.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">C++ Programming Modules</h3>
				<p>C++ is the advanced of C language, Once you will trained in C language our. Webgurukul team will make you expert in C++ also. We provide you best teaching Material with notes and PDF , Videos.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Graduating to C++ (Beginning)<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Oops</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Function Prototypes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Comments</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Typecasting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Void Pointers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	The :: operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	The Const Qualifier</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Reference variables</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Functions<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Function Prototypes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Function Overloading</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Default Arguments in Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Call by value, address &amp; reference</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Return by value, by address &amp; By reference</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>	Inline Functions</span></a></li>							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Classes in C++<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes in C++</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function Definition Outside The Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes and Constructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Destructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Copy Constructor</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The this Pointer</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New and delete Operators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using new and delete</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Malloc ( ) / free ( ) versus new/delete</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes, Objects and Memory</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Structures vs. Classes</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Miscellaneous Class Issues<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Static Class Data</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Static Member Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Conversion</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Friend functions &amp; friend Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Conversion between Objects of Different Classes</span></a></li>
							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Overloading operators<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading assignment operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading ++, --, +, -, *, /,<,> …. &amp; Logical operators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading operators between different objects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading '<<' and '>>' (stream operators)</span></a></li>				
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Inheritance<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Constructors in Inheritance</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Private Inheritance</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Protected Inheritance</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Functions That Are Not inherited</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Virtual Function</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pure virtual functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Virtual Functions in Derived Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Virtual Functions and Constructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Destructors and virtual Destructors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Virtual Base Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Abstract class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Abstract base class</span></a></li>

						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Advanced Features<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Classes Within Classes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Friend Functions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Friend classes</span></a></li>
							<!-- <li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Overloading << and >>.</span></a></li> -->
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Input / Output In C++ (File operations)<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Manipulators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>File I/O with Streams</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Opening and closing files</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating database with file Operation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Binary I/O</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Elementary Database Management</span></a>
							</li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Interacting with Text files and Non-text files</span></a>
								
							</li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating database with file operation</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: New Advanced Features<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Function templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Class templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exception handling</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Namespaces</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>RTTI (Runtime type information)</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>STL (Standard Template library)</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Dynamic cast operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Typeid operator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Typeinfo class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Data Structures with C++<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Sorting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Recursion</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Single linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Double linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Circular linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Traversing of linked lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Stacks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Queues</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding 2 lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Inserting a node in required position</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Deleting a node from required position</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: Discussion on FAQ</a>
						
					</li>
					
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to create a functional app from start to finish using industry standard practices.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Key coding concepts such as standard data types, variables, arithmetic expressions flow control, functions, classes, arrays, and pointers.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Computing theory and mathematical principles strictly as they apply to everyday C++ coding</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-cplusplusprogramming course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                </div>
                <P 	class="xlg">C++ Programming</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/programming2.pdf" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-cplusplusprogramming course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
            </div>
            <P 	class="xlg">C++ Programming</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/programming2.pdf" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<?php get_footer(); ?>