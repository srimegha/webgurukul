<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webgurukul
 */

?> 
 <!-- start register-section-->
    <section class="register-section bg-image">
        <div class="container">
            <h2 class="text-center color-white">Start Learning to Code Today</h2>
            <p class="sec-subheading text-center color-white">
                <strong>Join Us ! and Make your first step towards IT Industry.</strong>
            </p>
            <p class="text-center">
                <button class="btn fill-btn2 register-btn" data-toggle="modal" data-target="#register-modal">Enquire Now
                </button>
            </p>
        </div>
    </section>
    <!-- end register-section-->

    <!-- start footer -->
    <footer>
        <div class="footer-top">
            <div class="container">
                <div class="col-lg-8 col-md-7 links-footer hidden-xs clearfix">
                    <div class="row">                   
                        <div class="col-md-3 col-sm-3 footer-links no-padding">
                            <p class="sm footer-subheading">QUICK LINKS</p>
                            <ul class="xs list-unstyled">
                                <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/gallery">Gallery</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/blog">Our Blog</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/about">About Us</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/contact">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-md-5 col-sm-4 footer-courses1 no-padding hidden-xs">
                            <p class="sm footer-subheading">BASIC COURSES</p>
                            <ul class="xs list-unstyled">
                                <li><a href="<?php echo get_site_url(); ?>/web-designing-classes">Website Designing Classes</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/web-development-classes">Website Development Classes</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/it-internship-program-nagpur">IT Internship Program</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/digital-marketing-course-in-nagpur">Digital Marketing Course</a></li>
                                <li><a href="<?php echo get_site_url(); ?>/graphics-design-classes">Graphics Designing</a></li>    
                                <li><a href="<?php echo get_site_url(); ?>/technical-workshop">Technical Workshop</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 footer-courses2 no-padding hidden-xs">
                            <div class="row">
                                <p class="sm footer-subheading">CONTACT US</p>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-2 xs no-padding"><span class="wkl-call-answer footer-icon"></span></div>
                                    <div class="col-sm-11 col-xs-10 xs padding-right0"><p><strong>+91-8237733112 | +91-7387990061</strong></p></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-2 xs no-padding"><span class="wkl-black-back-closed-envelope-shape footer-icon"></span></div>
                                    <div class="col-sm-11 col-xs-10 xs padding-right0"><p><a href="mailto:edu@webgurukul.co.in">edu@webgurukul.co.in</a></p></div>
                                </div>
                            </div>
                            <div class="row subscribe-row">
                                <p class="sm footer-subheading">SUBSCRIBE</p>
                                <?php echo do_shortcode(' [email-subscribers desc="" group="Public"]'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-9 padding-left0 footer-inner hidden-sm hidden-xs">
                            <p class="copyright-text">© Copyright 2018 All Rights Reserved WebGurukul</p>
                            <ul class="list-unstyled connect">
                                <li>
                                    <a href="https://www.facebook.com/WebgurukulEdu/" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook">
                                        <span class="wkl-facebook"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/WebgurukulEdu" target="_blank" data-toggle="tooltip" data-placement="top" title="Twitter">
                                        <span class="wkl-social"><span class="path1"></span><span class="path2"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/webgurukul/?hl=en" target="_blank" data-toggle="tooltip" data-placement="top" title="Instagram">
                                        <span class="wkl-instagram-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/u/0/117078259563285069706" target="_blank" data-toggle="tooltip" data-placement="top" title="Google+">
                                        <span class="wkl-google-plus"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCvyg0Qdh5e4YNrXAe9PuC5w" target="_blank" data-toggle="tooltip" data-placement="top" title="Youtube">
                                        <span class="wkl-youtube"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>                     
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 footer-contact padding-right0">
                    <p class="sm col-xs-12 footer-subheading">ADDRESS</p>
                  
                    <div class="row">
                        <div class="col-sm-1 col-xs-2 xs"><span class="wkl-placeholder footer-icon"></span></div>
                        <div class="col-sm-11 col-xs-10 xs"><p><strong>IT Park Nagpur:</strong><br>Plot No. 11, Madhav Nagar, Behind Domino's Pizza, Near Mate Square, Nagpur - 440010.</p></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-xs-2 xs"><span class="wkl-placeholder footer-icon"></span></div>
                        <div class="col-sm-11 col-xs-10 xs"><p><strong>Near Medical Square, Nagpur:</strong><br>1st Floor, Plot No.351, Great Nag Road, Untkhana, Nagpur, Maharashtra - 440009.</p></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-xs-2 xs"><span class="wkl-placeholder footer-icon"></span></div>
                        <div class="col-sm-11 col-xs-10 xs"><p><strong>Wardha:</strong><br>2nd floor, Above Patni Multi-Speciality Dental Clinic, Near Wanjari Chowk, Wardha - 442001.</p></div>
                    </div>
                    <div class="row hidden-xs">
                        <div class="col-sm-1 col-xs-2 xs"></div>
                        <div class="col-sm-11 col-xs-10 xs"><a href="https://www.webakruti.com/" target="_blank" class="sm">Developed By Webakruti</a></div>          
                    </div>
                </div>

                <!-- below960 -->
                <div class="col-xs-12 padding-left0 footer-inner visible-sm visible-xs">
                    <p class="copyright-text">© Copyright 2018 All Rights Reserved WebGurukul</p>
                    <ul class="list-unstyled connect">
                        <li>
                            <a href="https://www.facebook.com/WebgurukulEdu/" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook">
                                <span class="wkl-facebook"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/WebgurukulEdu" target="_blank" data-toggle="tooltip" data-placement="top" title="Twitter">
                                <span class="wkl-social"><span class="path1"></span><span class="path2"></span></span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/webgurukul/?hl=en" target="_blank" data-toggle="tooltip" data-placement="top" title="Instagram">
                                <span class="wkl-instagram-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/u/0/117078259563285069706" target="_blank" data-toggle="tooltip" data-placement="top" title="Google Plus">
                                <span class="wkl-google-plus"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCvyg0Qdh5e4YNrXAe9PuC5w" target="_blank" data-toggle="tooltip" data-placement="top" title="Youtube">
                                <span class="wkl-youtube"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                            </a>
                        </li>
                    </ul>
                </div>
                
                <div class="row hidden-sm visible-xs">
                        <p class="sm"><a href="https://www.webakruti.com/" target="_blank">Developed By Webakruti</a></p>
                        
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

   
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
   
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/tether.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/mdb.js"></script>

    <!-- bxslider js -->
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.min_optimized.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

    <!-- scroll top button -->
    <button class="material-scrolltop" type="button" title="Scroll to Top"><span class="wkl-up-arrow"></span></button>

    <!-- scroll top js -->
    <script src="<?php bloginfo('template_directory'); ?>/js/material-scrolltop.js"></script>

    <!-- multi item slider- course details page -->
    <script src="<?php bloginfo('template_directory'); ?>/js/multislider.min.js"></script>

    <script type="text/javascript">
        //Start of Tawk.to Script
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5a853ede4b401e45400cf271/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        //End of Tawk.to Script

        $(document).ready(function () {
            setTimeout(function(){ 
                $("#front-loader-popup").show();
             }, 2000);
            $("#tclose").click(function(){
                $("#front-loader-popup").fadeOut();
            });
        });    

         $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    // script for bx-slider in 'tesimonial section' -home page      
 
        $(document).ready(function(){

              var maxSlides,
              
              width = $(window).width();
              if (width <= 1024) {
                  maxSlides = 1;
              }

              else {
                  maxSlides = 2;
              }

            $('#testimonial-slider').bxSlider({
                slideWidth: 550,
                minSlides: 1,
                maxSlides: maxSlides,
                moveSlides: 1,
                slideMargin: 30,
                auto:true,
                autoControls: true,
                nextText: '<img src="<?php bloginfo("template_directory"); ?>/img/right-arrow.png" height="35" width="20"/>',
                prevText: '<img src="<?php bloginfo("template_directory"); ?>/img/left-arrow.png" height="35" width="20"/>'
            });
        }); 

        // loader
        $(document).ready(function() {
            jQuery("#status").fadeOut();
            jQuery("#preloader").delay(500).fadeOut("slow");
            jQuery('html, body').css({'height':'auto','overflow':'auto'});
            $('body').materialScrollTop({
                revealElement: 'header',
                revealPosition: 'bottom',
                onScrollEnd: function() {
                //console.log('Scrolling End');
                }
            });
        });

    



    // script for bx-slider in 'other course section' -course details page

        $('#basicSlider').multislider({
              continuous: true,
              duration: 2000
            });
            $('#mixedSlider').multislider({
              duration: 750,
              interval: false
            });
      

    // script for open and close video on modal open and close

        autoPlayYouTubeModal();

          //FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
          function autoPlayYouTubeModal() {
              var trigger = $("body").find('[data-toggle="modal"]');
              trigger.click(function () {
                  var theModal = $(this).data("target"),
                      videoSRC = $(this).attr("data-theVideo"),
                      videoSRCauto = videoSRC + "?autoplay=1";
                  $(theModal + ' iframe').attr('src', videoSRCauto);
                  $(theModal + ' button.close').click(function () {
                      $(theModal + ' iframe').attr('src', videoSRC);
                  });
              });
          }
    
    // end script for open and close video on modal open and close

    // script for fixed nav-bar on scroll 

        $(document).ready(function() {
            $(window).scroll(function() {

                if ($(document).scrollTop() > 50) {
                  $('header').addClass('sticky');
                }

                else {
                  $('header').removeClass('sticky'); 
                }
            });
        });

    // end of script-shrink header on scroll

    // script for fixed div on scroll -course details page

        $(window).scroll(function () {
              var threshold = 50;
         
         if ($(window).scrollTop() >= threshold)
              $('#fix-div').addClass('fixed');
        else
          $('#fix-div').removeClass('fixed');
          
        // var check = $("#content").height() - $("#fix-div").height()-21;
        var check = $("#content").height() - 130;
          if ($(window).scrollTop() >= check)
              $('#fix-div').addClass('bottom');
          else
              $('#fix-div').removeClass('bottom');
      });
    
    // end of script-fixed div on scroll 

    // script for toggle list-course details page
    
        $(function() {
          $("#MainMenu > li > ul").hide();

          $('#MainMenu > li').click(function(e) {
            e.stopPropagation();
            var $el = $('ul',this);
          $('#MainMenu > li > ul').not($el).slideUp();
            $el.stop(true, true).slideToggle(400);
          });
          $('#MainMenu > li > ul > li').click(function(e) {
            e.stopImmediatePropagation(); 
          });

          // $('#MainMenu li').click(function(){
          //   $(this).find('i').toggleClass('wkl-add wkl-substract')
          // });
      });
    
    // end script for toggle list- course details page

    // script for counts

      (function ($) {
          $.fn.countTo = function (options) {
            options = options || {};
            
            return $(this).each(function () {
              // set options for current element
              var settings = $.extend({}, $.fn.countTo.defaults, {
                from:            $(this).data('from'),
                to:              $(this).data('to'),
                speed:           $(this).data('speed'),
                refreshInterval: $(this).data('refresh-interval'),
                decimals:        $(this).data('decimals')
              }, options);
              
              // how many times to update the value, and how much to increment the value on each update
              var loops = Math.ceil(settings.speed / settings.refreshInterval),
                increment = (settings.to - settings.from) / loops;
              
              // references & variables that will change with each update
              var self = this,
                $self = $(this),
                loopCount = 0,
                value = settings.from,
                data = $self.data('countTo') || {};
              
              $self.data('countTo', data);
              
              // if an existing interval can be found, clear it first
              if (data.interval) {
                clearInterval(data.interval);
              }
              data.interval = setInterval(updateTimer, settings.refreshInterval);
              
              // initialize the element with the starting value
              render(value);
              
              function updateTimer() {
                value += increment;
                loopCount++;
                
                render(value);
                
                if (typeof(settings.onUpdate) == 'function') {
                  settings.onUpdate.call(self, value);
                }
                
                if (loopCount >= loops) {
                  // remove the interval
                  $self.removeData('countTo');
                  clearInterval(data.interval);
                  value = settings.to;
                  
                  if (typeof(settings.onComplete) == 'function') {
                    settings.onComplete.call(self, value);
                  }
                }
              }
              
              function render(value) {
                var formattedValue = settings.formatter.call(self, value, settings);
                $self.html(formattedValue);
              }
            });
          };
          
          $.fn.countTo.defaults = {
            from: 0,               // the number the element should start at
            to: 0,                 // the number the element should end at
            speed: 1000,           // how long it should take to count between the target numbers
            refreshInterval: 100,  // how often the element should be updated
            decimals: 0,           // the number of decimal places to show
            formatter: formatter,  // handler for formatting the value before rendering
            onUpdate: null,        // callback method for every time the element is updated
            onComplete: null       // callback method for when the element finishes updating
          };
          
          function formatter(value, settings) {
            return value.toFixed(settings.decimals);
          }
        }(jQuery));

        jQuery(function ($) {
          // custom formatting example
          $('.count-number').data('countToOptions', {
          formatter: function (value, options) {
            return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, '');
          }
          });
          
          // start all the timers
          $('.timer').each(count);  
          
          function count(options) {
          var $this = $(this);
          options = $.extend({}, options || {}, $this.data('countToOptions') || {});
          $this.countTo(options);
          }
        });
    

    // responsive nav open close script
 
        $("#menu a").click(function(event){
          // event.preventDefault();
          if($(this).next('ul').length){
            $(this).next().toggle('fast');
            $(this).children('i:last-child').toggleClass('dropdown-arrow-rotate');
          }
        });
    

        function openNav() {
            document.getElementById("menu").style.right = "0";
            $('.overlay-div').addClass('overlay');
        }

        function closeNav() {
            document.getElementById("menu").style.right = "-280px";
            $('.overlay-div').removeClass('overlay');
        }

        $(document).ready(function(){
            $('#menu-item-269 > ul').addClass('dropdown1 dropdown-menu multi-level');
            $('#menu-item-269').addClass('dropdown'); 

            $('#menu-item-104').addClass('dropdown-submenu');
            $('#menu-item-104 > ul').addClass('dropdown-menu');

            $('#menu-item-103').addClass('dropdown-submenu');
            $('#menu-item-103 > ul').addClass('dropdown-menu');
            $('#menu-item-107').addClass('dropdown-submenu');
            $('#menu-item-107 > ul').addClass('dropdown-menu');
            $('#menu-item-105').addClass('dropdown-submenu');
            $('#menu-item-105 > ul').addClass('dropdown-menu');
            $('#menu-item-106').addClass('dropdown-submenu');
            $('#menu-item-106 > ul').addClass('dropdown-menu');
            $('#menu-item-129').addClass('dropdown-submenu');
            $('#menu-item-129 > ul').addClass('dropdown-menu');

        });
    
        $(document).ready(function(){
            width = $(window).width();
            if (width <= 800) {
                $('.slider-overlay').addClass('docFade');
            }else{
                $('.slider-overlay').removeClass('docFade');
            }
        });

        $('#es_txt_email_pg').click(function(){
            $('.es_shortcode_form_email').hide();
        });

    </script>
    <?php wp_footer(); ?> 
  </body>
</html>