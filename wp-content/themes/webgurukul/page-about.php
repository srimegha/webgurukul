<?php get_header(); ?>


<!-- banner -->
<section class="about-banner link-page-banner bg-image">
	<h2 class="banner-heading">About Us</h2>
	<ul class="breadcrumb">
		<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
	</ul>
</section>
<!-- end banner -->

<!-- about-us content-->
<section class="about-section1 link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading text-center">Welcome to Webgurukul, We are Company Trainers</h1>
		 <div class="col-xs-12 section1-para">
            <p class="text-center">Webgurukul is a Leading IT Training Institute in Nagpur It is also located in Wardha. We are building the students technical knowledge & trained them on different Web Development Courses and Web Designing Courses, in coordination with that we are building their confidence to stand them in a competitive world. We are bridging the gap between students & professional industries. Our Education programs provide recreational and innovative techniques for Web Designing Course and Web Development Course, Mobile App Designing and Development courses, Digital Marketing Course to compose the student from basic to the scholar.</p>
            <p class="text-center sec-para">At Webgurukul we especially concentrate on the different channels in a necessity of the student to achieve his ultimate goal of getting dream job which is the uttermost need of a youngster. In this program is one of the very new initiatives to empower the education of Nagpur & Wardha students at very affordable prices which the Nagpur & Wardha student deserves. Here we offer students expertise training on a different concept of Software Development Industry.</p>
        </div>
	</div>
</section>

<section class="about-section2 padding-top-bot grey-section">
	<div class="container">
		<h2 class="sec-heading col-xs-12">Who We Are? </h2>
		<div class="row visible-md visible-lg">
			<div class="col-md-7 clr-green">
				<p>
					We are the leading IT Training company in the Central India. We are located at Nagpur & Wardha. We are providing job oriented technical education which necessary for the students and job seekers nowadays. We are trying to bridge the gap between students (freshers) and professional industries. We are making the change in traditional education system by making it more practical and easy to learn approach.
				</p>
				<h2 class="course-details-headings no-padding">Why We are different?</h2>
				<ul class="list-unstyled no-padding check-list">
					<li>100% Practical Sessions</li>
					<li>Live and Practical Projects</li>
					<li>Daily doubt Solving Session</li>
					<li>Interview Preparation, Personality Development Sessions</li>
					<li>Personal Attention</li>
					<li>Full Time Internship Program</li>
					<li>Job Assistance</li>
				</ul>
			</div>
			<div class="col-md-5">
				<img class="img-responsive imgwidth-100" src="<?php bloginfo("template_directory"); ?>/img/about-us-img2.jpg" alt="about-us-img" />
			</div>
		</div>
		<!-- below 720 -->
		<div class="row visible-sm visible-xs abt-xs-content clr-green">
			<div class="col-xs-12">
				<img class="img-responsive imgwidth-100 who-we-image" src="<?php bloginfo("template_directory"); ?>/img/about-us-img2.jpg" alt="about-us-img" />
				<p>
					We are the leading IT Training company in the Central India. We are located at Nagpur & Wardha. We are providing job oriented technical education which necessary for the students and job seekers nowadays. We are trying to bridge the gap between students (freshers) and professional industries. We are making the change in traditional education system by making it more practical and easy to learn approach.
				</p>
				<h2 class="course-details-headings no-padding">Why We are different?</h2>
				<ul class="list-unstyled no-padding check-list">
					<li>100% Practical Sessions.</li>
					<li>Live and Practical Projects</li>
					<li>Daily doubt Solving Sesion</li>
					<li>Interview Preparation, Personality Development Sessions</li>
					<li>Personal Attention</li>
					<li>Full Time Internship Program</li>
					<li>Job Assistance</li>
				</ul>
	        </div>
		</div>
	</div>
</section>

<section class="about-section3 padding-top-bot">
	<div class="container">
		<h2 class="text-center">Meet Our Trainers</h2>
		<p class="sec-subheading text-center">Our  trainers shares a passion for learning, technology.</p>
		<?php               
            $args = array(
                'post_type'=> 'web_trainers'      
            );
            query_posts( $args );
        ?>
        <div class="custom-flex">
	        <?php  while ( have_posts() ) : the_post(); ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="trainer-photo-circle">
					<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>" alt="about-us-img" />
					<p class="lg1"><?php the_title(); ?></p>
					<p><?php the_content(); ?></p>
				</div>
			</div>
		    <?php endwhile; ?>
		</div>
	</div>
</section>


<?php get_footer(); ?>