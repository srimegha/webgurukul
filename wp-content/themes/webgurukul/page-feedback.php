<?php get_header(); ?>

<section class="registration-page padding-top-bot grey-section feedback-page">
	<div class="container">
        <div class="reg-form col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 no-padding">
        	<div class="col-sm-3 hidden-xs no-padding">
                <div class="col-xs-12 no-padding rocket-div">
                    <div class="tranparent-arrow"></div>
                    <img class="img-responsive webgurukul-icon" src="<?php bloginfo('template_directory'); ?>/img/curved-icon.svg" />
                    <img class="img-responsive rocket-img" src="<?php bloginfo('template_directory'); ?>/img/rocket.svg" />
                </div>
            </div>
            <div class="col-sm-9 col-xs-12 feedback-form">
                <h2 class="clr-orange col-xs-12">Feedback Form</h2>
				<?php echo do_shortcode( '[RICH_REVIEWS_FORM]') ?>	
            </div>
            
    		
         </div>
	</div>
</section>

 <?php get_footer(); ?>
 <script>
 	$(document).ready(function(){
 		$("#submitReview").addClass("submit-btn fill-btn1");
 		$("textarea.rr_large_input").addClass("form-control validate");
 	});
 </script>
