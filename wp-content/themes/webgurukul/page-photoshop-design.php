<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Photoshop</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Photoshop Training In Nagpur</h1>
				<p>Photoshop is the industry-standard image editing software and is used worldwide by photographers, graphic designers, and UI/UX designer to perfect their digital images and designs. With this course and training from Webgurukul, you can learn the skills to become a Photoshop expert.</p>

				<p>It is the present driving picture altering project of decision for visual architects, picture takers, and web originators, in this course you going to learn image editing, manipulate photographs, website template designing and many more.</p>

				<p>If you are a student, photographer, UI/UX designer, or web designer, learning how to use Photoshop is a must! Whether you are new to Photoshop or a seasoned user, Photoshop courses at Webgurukul will provide you will valuable tips and tricks to improve efficiency and make your work come to life. Our basic Photoshop instructional class expects to build up your commonality with the workspace, devices, and elements.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<!-- <p class="course-details-headings">Who can Apply?</p>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul> -->

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<!-- <li><span>Basics of Photoshop else alright if don’t know.</span></li> -->
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Photoshop Modules</h3>
				<p>Photoshop is the industry-standard image editing software, it lets you enhance, retouch, and manipulate photographs and also for web designs like template designing, graphics. Photoshop allows you to do any designing workings of your imagination and showcase them for the world to see.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to Adobe Photoshop CS6<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>About Photoshop</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Navigating Photoshop</span>
							</a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Menus and Panels</span></a>
							</li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Understanding Workspace</span></a>
							</li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Opening new files</span></a>
							</li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Opening existing files</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Exploring the Toolbox<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with all Selection tools</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic Painting tools</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>All Photo editing tools</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring basics of Pen tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to create shapes using Shape tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating text with Text tool</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Getting Started With Layers <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Understanding the Background Layer</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating, Selecting, Linking &amp; Deleting Layers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Locking &amp; Merging Layers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Copying Layers, Using Perspective &amp; Layer Styles</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Filling &amp; Grouping Layers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Blending Modes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Blending Modes, Opacity &amp; Fill</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating &amp; Modifying Text</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Working with all types of Images<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Resizing and cropping images</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic editing in images</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to make GIF</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic Adjustment in images</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Removing background of images</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Saving images in different format</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Working with Masks and Quick mask Mode<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring mask options and working</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating Clipping mask</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating Layer mask </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Text masking using Layer </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Removing background using mask </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Creating logo in Photoshop<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Type of Logos</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating logos with Shape</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating logos with Pen tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Text logos using text styling</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Styling logos with Layer Style</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Working with main menu options<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring File, Edit Option</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Understanding Image, Layer and Type Option</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Getting Started with Select and Filters Option</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Template Designing<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to template Layout</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with template Wireframe</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Starting with section header, slider, body, footer</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Text Typography, Icons, Styling</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Completing with Template Designing<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Giving Effects and Text adjustments</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Grouping of Layers</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Saving the work <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Saving with Different File Formats</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Saving for Web &amp; Devices</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Basic editing of images and manipulate photograph.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use of all tools in Photoshop with their functionality.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Removing object from images or making it blur.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Resizing images with dimension and size.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Basic template designing for portfolio website.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Basics of image blending options.</p>
						</div>
					</div>
				</div>
				
			</div>


		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-Adobe_Photoshop course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div>
                <P 	class="xlg">Photoshop</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration-page" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-Adobe_Photoshop course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
            </div>
            <P 	class="xlg">Photoshop</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
<?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>