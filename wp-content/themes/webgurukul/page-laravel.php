<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Laravel</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Laravel Training In Nagpur</h1>
				<p>Webgurukul is an Official partner of WordPress Community Nagpur our teaching faculty is an Certified and Official Member of WordPress Organization of Nagpur, Webgurukul is a First WordPress Training Center in Nagpur ,we offered only JOB oriented Advance IT courses. We trained 3000+ Students in Nagpur and Wardha. Our second branch is in Wardha</p>
				<p>In Nagpur there are so many IT Training Institute they "ONLY TEACH" But webgurukul make you  "EXPERT , MASTER, PROFFESIONAL" in WordPress and other IT Courses. We provide Internship Program to our trainees. Same training we Provide IT Training in Wardha also.</p>
				<p>All IT Companies have a Huge Demand for WordPress Developer, All IT Companies offering Good Packages for fresher's and Professionals . Don't waste your time and join WordPress Training in Webgurukul.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>Graduate and Undergraduate</span></li>
					<li><span>Freshers</span></li>
					<li><span>Professionals</span></li>
					<li><span>Job Seekers</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>Basics of HTML, PHP.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Laravel Modules</h3>
				<p>Laravel has become one of the famous frameworks and a sought after skill for PHP developers. Its clean and elegant syntax coupled with the powerful toolkit makes it an ideal choice for modern web developers. This powerful technology boast of features such as clean routing, elegant queue library, ORM ,simple authentication and a large developer community. </p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Getting Started with Laravel<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Installation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Configuration</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Directory Structure</span></a></li>						
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Homestead</span></a></li>							
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Valet</span></a></li>							
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Deployment</span></a></li>							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Architecture Concepts<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Request Lifecycle</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Service Container</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Service Providers</span></a></li>						
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Facades</span></a></li>						
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Contracts</span></a></li>						
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: The Basics of Laravel <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Routing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Middleware</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSRF Protection</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Controllers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Requests</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Responses</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Views</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>URL Generation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Session</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Validation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Errors &amp; Logging</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Frontend <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Blade Templates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Localization</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Frontend Scaffolding</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Compiling Assets</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Security <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Authentication</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>API Authentication</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Authorization</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Encryption</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Hashing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Password Reset</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Digging Deeper <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Artisan Console</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Broadcasting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Cache</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Collections</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Events</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>File Storage</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Helpers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Mail</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Notifications</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Package Development</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Queues</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Task Scheduling</span></a></li>
						</ul>
					</li>	
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Database <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Getting Started</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Query Builder</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pagination</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Migrations</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Seeding</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Redis</span></a></li>
						</ul>
					</li>	
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Eloquent ORM <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Getting Started</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Relationships</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Collections</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Mutators</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>API Resources</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Serialization</span></a></li>
						</ul>
					</li>	
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Testing <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Getting Started</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>HTTP Tests</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Browser Tests</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Database</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Mocking</span></a></li>
						</ul>
					</li>			
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Develop Apps using Laravel PHP framework</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Learn about Laravel features such as ORM, modular packaging system and Query Builder</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Learn all about MVC architecture</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Learn to develop scalable and maintainable apps</p>
						</div>
					</div>
				</div>			
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-laravel-logo course-icon3"></span>
                </div>
                <P 	class="xlg">Laravel</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-laravel-logo course-icon3"></span>
            </div>
            <P 	class="xlg">Laravel</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
<?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>