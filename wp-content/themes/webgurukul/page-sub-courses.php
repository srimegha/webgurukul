<?php /* Template Name: Sub Courses */
 get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<p class="banner-heading text-uppercase"><?php the_title(); ?></p>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1><?php the_title(); ?> Training In Nagpur</h1>
				<p>Bootstrap is the most popular HTML, CSS, and JavaScript framework for developing responsive, mobile-first web sites. A technique of loading a program into a computer by means of a few initial instructions which enable the introduction of the rest of the program from an input device</p>
				<p>Bootstrap Training is important because, bootstrap helps to make Best Responsive web Design. Bootstrap is combination of HTML, CSS3, Jquery, Javascript Framework. Bootstrap is responsive, mobile-first, prevailing, and front-end framework, which is developed along with CSS, JavaScript, and HTML. Bootstrap has many benefits from scratch for every web development project, and one such reason is the huge number of resources accessible for Bootstrap. Bootstrap course is an 100% JOB oriented Course. On this technology having so many openings in Nagpur IT Park and All Over India . All companies offering good Salary to fresher also. 
				</p>
				<p>
				Webgurukul IT Training Institute trained 3000+ Students in Nagpur and wardha. All our students placed in Reputed IT Companies and In Organization all over India. We have an expert faculty for Bootstarp Training In Nagpur, there are So many coaching they doing business only for money. But webgurukul Don't, We will train you like Expert, After joining this Course what you will See?, You will see you become a professional in Bootstrap Technology. JOIN Webgurukul TODAY.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<p class="course-details-headings">Who can Apply?</p>
				<ul>
					<li><span>Graduate and Undergraduate</span></li>
					<li><span>Fresher’s</span></li>
					<li><span>Professionals</span></li>
					<li><span>Job Seekers</span></li>
				</ul>

				<p class="course-details-headings">Required Knowledge</p>
				<ul>
					<li><span>Basic Knowledge of HTML ad CSS</span></li>
					<li><span>HTML 5</span></li>
					<li><span>CSS 3</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<p class="course-details-headings">Bootstrap Modules</p>
				<p>CSS is a languages that can use to build Style in website. In these courses, you’ll learn the all basics of CSS, build your first beutiful website, and then review some of the current CSS3 best practices. Be ready with all concepts of CSS and CSS3.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Customize Bootstrap</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Comprehend the Carousel</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Comprehend the Typeahead</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Comprehend the Model</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Comprehend the Bootstrap File Structure</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Comprehend the Default Grid System</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Comprehend the Fluid Grid System</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Comprehend the Responsive Design</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Comprehend the Dropdown Menus</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Comprehend the Button Groups</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: Comprehend the Navbar, Breadcrumbs, Pagination, Labels, and Badges</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: Comprehend the Typographic Elements, Thumbnails, Alerts and Progress Bars</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 13: Comprehend the Media Object, Typography, and Tables</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 14: Comprehend the Forms</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 15: Comprehend the Button</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 16: Comprehend the Images</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 17: Comprehend the Icons</a>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<p class="course-details-headings">What will you are going to Learn?</p>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="course-details-review course-details-block col-xs-12">
				<p class="course-details-headings">Course Reviews</p>
				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<p class="text-center"> 
					<a href="JavaScript:Void(0);" class="btn border-btn1 courses-border-btn1">See More</a>
				</p>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-Boostrap_logo course-icon3"><span class="path1"></span><span class="path2"></span></span>
                </div>
                <P 	class="xlg"><?php the_title(); ?></P>
                <p class="text-center">
					<a href="<?php echo do_shortcode('registration') ?>" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<<span class="wkl-Boostrap_logo course-icon3"><span class="path1"></span><span class="path2"></span></span>
            </div>
            <P 	class="xlg">Bootstrap</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->

<!-- other course slider -->
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
               		<span class="wkl-web-development-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                </div>
			               
                <h3>Website Development<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="web-development.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-Graphics-design-course course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                </div>
           
                <h3>Graphics Design<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="digital-marketing.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
	            <div class="course-circle">
	           		 <span class="wkl-Software-course course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
	            </div>
	       
	            <h3>Software<br>Course</h3>
	      
	            <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
	            <a href="software-course.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
	            <div class="course-circle">
	           		<span class="wkl-advance-c-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
	            </div>
	       
	            <h3>Programming<br>Languages</h3>
	      
	            <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
	            <a href="programming-language.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-digital-marketing-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
                </div>
           
                <h3>Digital Marketing<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="web-design.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>

		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-employee course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
                </div>
           
                <h3>Internship<br>Program</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="internship-program.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>

		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-coporate-training-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div>
           
                <h3>Corporate<br>Training</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="corporate-tranning.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>

		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-PHP-Course course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                </div>
           
                <h3>Complete PHP<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="complete-php-course.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
	     </div>
                       
        </div>
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

<!-- below768 -->
<section class="other-course-section1 padding-top-bot">
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<li><a href="web-development.php">Website Development Course</a></li>
				<li><a href="internship-program.php">Internship Program</a></li>
				<li><a href="programming-language.php">Programming Languages</li>
				<li><a href="digital-marketing.php">Digital Marketing Course</li>
				<li><a href="corporate-tranning.php">Corporate Training</li>
				<li><a href="graphics-design.php">Graphics Design Course</li>
				<li><a href="software-course.php">Software Course</li>
				<li><a href="complete-php-course.php">Complete PHP Course</li>
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>