<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Complete PHP Course</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn Complete PHP Course in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				There are many Web Designing Courses in Nagpur. But Webgurukul is only the best destination webgurukul is a best IT Training institute in Nagpur that teaches you from very basic in Web Designing Course practically. Here we make you master in Web Designing by providing excellent coaching with live practical orientation. The advance Web Design Course is for those who are looking bright career in IT Sector, Webgurukul is the best place to make your bright career in IT Field.
			</p>
			<p>
				If you are passionate about web designing Webgurukul is a perfect place. We will provide perfect training and Guidance, this Web Design Course in Nagpur and Wardha. Join Today Best IT Training Institute Webgurukul and being a master in Web Designing.
			</p>
			<p>
				Webgurukul is a Leading IT Training Institute in Nagpur, We offers Job oriented advanced Web Design Course in this course you will teach how to design Responsive and Attractive website. Web Designing course content and syllabus based on students requirement to achieve everyone's career goal. If you are thinking career in IT Sector, Join job oriented Web Design Course at Webgurukul. Our Team will make you Best Coder in Web Design. 
			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-PHP-Course course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot grey-section">
	<div class="container">
		<h2 class="text-center">Get your Complete PHP Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class="row course-row no-padding">
            <div class="col-md-4 col-sm-12 margin-right-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-cprogramming course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>C Programming</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="c-programming.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-cplusplusprogramming course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>C++ Programming</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="cpp-programming.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-Oopsconcept course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>OOP’S Concept</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="oops-concept.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>  
	</div>
</section>
<!-- end courses-module section -->

<!-- what to learn section -->
<section class="what-to-learn padding-top-bot">
	<div class="container">
		<h2>What will you are going to Learn?</h2>
		<p class="sec-subheading">Our Expert Faculty will teach you all Complete PHP Course modules.</p>
		<!-- row-1 -->
		<div class="row what-to-learn-row">
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Web Designing Concept</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Website Layout</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Attractive website Theme </p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
		</div>
		<!-- row-2 -->
		<div class="row what-to-learn-row">
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Antinational Website Design</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Photo Base Website Making</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Single Page website Designs</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
		</div>
		<!-- row-3 -->
		<div class="row what-to-learn-row">
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Attractive Forms Designs</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Responsive Web Design</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Creative Design Making </p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end what to learn section -->


<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Complete PHP Course</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Back end Designer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="contact.php" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!-- end course page register and contact section -->

<!-- course page review section -->
<section class="course-reviews padding-top-bot">
	<div class="container">
		<h2 class="review-heading">Course Rewiews (174)</h2>
		<div class="row course-reviews-row">
			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row course-reviews-row">
			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>
		</div>
		<p class="text-center"> 
			<a href="JavaScript:Void(0);" class="btn border-btn1 courses-border-btn1">See More</a>
		</p>
	</div>
</section>
<!-- end course page review section -->

<?php get_footer(); ?>