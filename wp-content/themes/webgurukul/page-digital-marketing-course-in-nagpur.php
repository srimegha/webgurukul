<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Digital Marketing Course</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section> 
<!-- end banner -->

<!-- course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn Digital Marketing Course in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				Webgurukul Digital Marketing in Nagpur, this program is a good start for students who are looking for their career to start in the field of Digital Marketing. In this program we helps the student to deliver themselves in the industry of Digital Marketing with many relevant skills. Here trainers are working with more than 5+ years of Digital Marketing experts, which help them a lot in Technical knowledge boosting. Digital Marketing Course Nagpur program is a full-time training program for trainees where they work on live projects running under Webgurukul.</p>
			<p>
				Webgurukul is announcing Digital Marketing Course . Programs for Engineering Students (BE), MCA students, BCA students, BTech Students, This internship training programs for computer science, Information Technology and Other streams of engineering.
			</p>
			<p>
				In this Digital Marketing Course Nagpur training program we will cover all technologies related to Branding like SEO, SMO, Google Adwords,E-Mail Marketing, Google Analytics Tool , Google Webmaster Tool, Google Adwords, Affiliate Marketing, Digital Marketing Interview Training.
			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<!-- <span class="wkl-digital-marketing-icon course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span> -->
				<div class="course-circle1">
           			<span class="wkl-digital-marketing course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></span>
            	</div>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-DigitalMarketingCourse.pdf" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!-- end course section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot grey-section">
	<div class="container">
		<h2 class="text-center">Get your Digital Marketing Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class="row course-row no-padding">
            <div class="col-md-4 col-sm-12 margin-right-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-seo course-icon"></span>
                    </div>
                    <!-- subheading -->
                    <h3>Advance SEO 2017</h3>
                    <!-- para -->
                    <p>SEO is a process to optimize your website web pages according to Search Engine all Search Algorithms and Parameters should be in a Proper manner in your website, It’s a processes give a boost to your website in Search Engine and Visible your all Web Pages in Search Engine</p>
                    <a href="advance-seo.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-google-analytics course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Google Analytics</h3>
                    <!-- para -->
                    <p>Do you want to know more about your visitors and how your Website getting More Traffic? Whether you run a website , Google Analytics is the Best Industry for Tracking, Analyzing and Reporting Site data. This tool is free for use , Its an Official Tool of Google. There are so many parameters are you will get demographics, Interest, Geo, Locations and many more</p>
                    <a href="google-analytics.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-google-webmaster course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Google Webmaster</h3>
                    <!-- para -->
                    <p>Google Webmaster Tools is a free tool its an official tool of Google Community it helps to maintain your website's performance in search Engine Results. Google Offered free service to anyone who have a website, Google Webmaster Tools (GWT) is a conduit of information from the largest search engine and helping you uncover issues that need fixing.</p>
                    <a href="google-webmaster.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>
        <div class="row course-row no-padding">
            <div class="col-md-4 col-sm-12 margin-right-15">
               <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-google-adwords course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Google AdWords</h3>
                    <!-- para -->
                    <p>It is a Paid Ad Campaign , In this platform, we have to invest budget for our Business ad Google Will decide on which position your Ad will be placed , For this, your Company landing page should be relevant to your Product and High quality Content. We will help you grow your Business</p>
                    <a href="google-adwords.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-smo course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Social Media Marketing</h3>
                    <!-- para -->
                    <p>Social Media Optimization is a Beneficial and Strong Platform to increase your Brand awareness and It helps to increase Business also. Averagely people spend their 60% time on Social Media, So Social Media is good way to increase your brand Value, Come today at webgurukul</p>
                    <a href="social-media-marketing.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div> 
            </div>
            <div class="col-md-4 col-sm-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-email-marketing course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Email Marketing</h3>
                    <!-- para -->
                    <p>Email Marketing is a vibrant and powerful way to connect with people. All leading company generally connect with their customer via E-mail Only , And its very help full to the customer also they get new updates by E-mail .We respect your imaginations, making a copy of the same is challenges.</p>
                    <a href="email-marketing.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>      
	</div>
	<section class="what-to-learn padding-top-bot">
		<div class="container">	
			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Digital Marketing Modules</h3>
				<p>Learn the basics of proven digital marketing strategy which simply works. Step by step we will learn how to create a high-converting web page and exciting free content, so you can start building a list of prospects. We will cover simple and low-cost ways of generating high quality traffic like Facebook ads, social media contests, search engine marketing.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to Digital Marketing <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Digital Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Benefits of Digital Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Digital Marketing Vs Traditional Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Scope Of Digital Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Career opportunities in digital marketing</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Advanced SEO 2017<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to SEO</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Research and Analysis of Keywords</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>ON page Optimization</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>OFF Page Optimization</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: E-Mail Marketing<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Email Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Benefits of email marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic terminology in email marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Email Marketing Software’s</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Designing Newsletters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>E-mail Marketing Guidelines</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How many E-mail Will you send From one E-mail ID?</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Social Media Optimization<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of Social Media</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why Social Media Is Important</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Benefits of using SMM</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Social Media Statistics</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why use Social Media Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Social Media Strategy</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Impact of Social Media on SEO</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Top Social media platform facebook, Google+, twiter, Youtube, Instagram, Pinterest, Reddit usages.</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Additional top 10+ Social media platform usages.</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Google Analytics Tools<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Google Analytics</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why Google Analytics</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Installing Analytics in Site</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Generating Reports</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Designing Newsletters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Complete Parameters of Tool</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Google Webmaster Tools<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Google Webmaster Tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why Google Webmaster Tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Installing Webmaster Tool in Site</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Generating Reports</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Complete Parameters of Tool</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Google Adwords<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Google Adwords</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why Google Adwords</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Google Adwords Network</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Search Engine Ads and Parameters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Display Network Ads and Parameters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Mobile Ads and Parameters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Shopping Ads and Parameters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Video Ads and Parameters</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Affiliate Marketing<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Affiliate Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why Affiliate Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Importance of Goals and Conversions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How Affiliate Marketing works</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How Affiliate Marketing websites</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Digital Marketing Interview Questions <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to apply for Digital Jobs</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Resume Making for Special Digital Marketing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Naukri.com Profile for Digital Marketing Specialist</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to face Digital Marketing Interview</span></a></li>
						</ul>
					</li>				
				</ul>
			</div>
		</div>
	</section>
</section>
<!-- end courses-module section -->

<!-- what to learn section -->

<!-- end what to learn section -->

<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Digital Marketing Course</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Digital Marketing expert in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="<?php echo get_site_url(); ?>/contact" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!-- end course page register and contact section -->



<?php get_footer(); ?>