<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webgurukul
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable= no">
    <link rel="short icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/img/webgurukul-fevicon.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">
     <!-- material design css -->
    <link href="<?php bloginfo('template_directory'); ?>/css/mdb.css" rel="stylesheet">
    <!-- bx-slider -->
    <link href="<?php bloginfo('template_directory'); ?>/css/jquery.bxslider.css" rel="stylesheet" />
    <!-- multiitem slider css -->
    <link href="<?php bloginfo('template_directory'); ?>/css/multislider.css" rel="stylesheet">
    <!-- base css -->
    <link href="<?php bloginfo('template_directory'); ?>/css/base.css" rel="stylesheet">
    <!-- scroll top -->
    <link rel="stylesheet" type="text/css" 
    href="<?php bloginfo('template_directory'); ?>/css/material-scrolltop.css">
    <!-- main css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body class="no-padding" <?php body_class(); ?>>
    <!-- header -->
    <header>
        <div class="top-strip">
            <div class="container">
                <ul class="list-unstyled pull-left top-contact">
                    <li>
                        <span class="wkl-phone-call glaphicon phone-icon1"></span>
                    </li>
                    <li class="sm number">+91-8237733112 | +91-7387990061</li>
                    <li class="hidden-xs">
                        <span class="wkl-e-mail-envelope glaphicon mail-icon1"></span>
                    </li>
                    <li class="sm mail hidden-xs">
                        <a href="mailto:edu@webgurukul.co.in">edu@webgurukul.co.in</a>
                    </li>
                </ul>
                <ul class="list-unstyled pull-right connect hidden-xs">
                    <li>
                        <a href="https://www.facebook.com/WebgurukulEdu/" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Facebook"><span class="wkl-facebook">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/WebgurukulEdu" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Twitter">
                            <span class="wkl-social"><span class="path1"></span><span class="path2"></span></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/webgurukul/?hl=en" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Instagram">
                            <span class="wkl-instagram-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/u/0/117078259563285069706" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Google+">
                            <span class="wkl-google-plus"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCvyg0Qdh5e4YNrXAe9PuC5w" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Youtube">
                            <span class="wkl-youtube"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div> <!-- end top-strip -->

        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <!-- header logo -->
                    <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                        <img class="webgurukul-logo" src="<?php bloginfo('template_directory'); ?>/img/webgurukul-logo.svg"  alt="WebGurukul" title="WebGurukul" />
                    </a>
                </div>

                <div class="navbar-collapse collapse hidden-xs hidden-sm no-padding">
                   <!--  <ul class="nav navbar-nav navbar-right"> -->
                        <?php
                                $args = array(              /*This code show the menu on front page through dashboard*/ 

                                'theme_location' => 'primary',
                                'menu_class' => 'nav navbar-nav navbar-right'                               
                                );?>
                                <button class="btn border-btn1 navbar-btn visible-lg visible-md" data-toggle="modal" data-target="#register-modal">Enquiry
                        </button>
                       <?php wp_nav_menu($args); ?>
                       
                </div>
                  
                <!--responsive menu vertical -->
                <span class="overlay-div"></span> 
                <div id="menu" class="sidenav-left visible-xs visible-sm hidden-md">
                    <button class="closebtn" onclick="closeNav()">&times;</button>
                     <?php
                                $args = array(              /*This code show the menu on front page through dashboard*/ 

                                'theme_location' => 'mobile_nav',
                                'menu_class' => 'menu-ul1'                               
                                );
                                wp_nav_menu($args); ?>
                </div>
                <button id="showmenu" onclick="openNav()" class="visible-xs visible-sm"> <i class="wkl-menu"></i> </button> 
                <!--end responsive menu vertical -->   
            </div> <!-- end container -->
        </nav>
        
    </header><!-- end header -->

    <!-- Modal -->
    <div class="modal fade" id="register-modal" role="dialog">
        <div class="modal-dialog register-modal ">
            <!-- Modal content-->
            <div class="modal-content clearfix">
                <div class="col-xs-2 no-padding">
                    <div class="col-xs-12 no-padding rocket-div">
                        <div class="tranparent-arrow"></div>
                        <img class="img-responsive webgurukul-icon" src="<?php bloginfo('template_directory'); ?>/img/curved-icon.svg" />
                        <img class="img-responsive rocket-img" src="<?php bloginfo('template_directory'); ?>/img/rocket.svg" />
                    </div>
                </div>
                <div class="col-md-10 col-sm-12 col-xs-12">
                    <div class="modal-header">
                        <button type="button" class="close resgister-close" data-dismiss="modal"><span class="wkl-letter-x"></span></button>
                        <h4 class="modal-title">Enquiry Form</h4>
                    </div>
                    <div class="modal-body clearfix">
                    <?php echo do_shortcode( '[contact-form-7 id="242" title="Enquiry Form"]') ?>
                       
                    </div>
                </div>
            </div> <!-- end modal content -->
        </div><!-- end modal dialog -->
    </div> <!-- end modal -->
    <a href="<?php echo get_site_url(); ?>/feedback">
        <div class="feedback-section">
            <span class="wkl-feedback-icon2 feedback-icon bounce"></span>       
            <span class="lg brd">Give Your Feedback!</span>
        </div>
    </a>
    <?php if(is_front_page()){ ?>
    <?php               
                    $args = array(
                        'post_type'=> 'pop_notification'     
                    );
                    query_posts( $args );
                      while ( have_posts() ) : the_post();
                  ?>
    <div class="banner-pop" id="front-loader-popup" style="display:none;">
        <div class="container col-md-12">
            <div class="banner-pop-blog text-center">
            <div id="tclose">X</div> 
                <a href="<?php get_site_url(); ?>/registration" target="_blank">
                    <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive" alt="<?php the_title(); ?>">
                </a>
            </div>
        </div>   
    </div>
    <?php endwhile; ?>
  <?php }?> 
