<?php get_header(); ?>


<!-- banner -->
<section class="gallery-banner link-page-banner bg-image">
	<h2 class="banner-heading">Gallery</h2>
	<ul class="breadcrumb">
		<?php if ( function_exists('yoast_breadcrumb') ) 
			{
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} 
		?>
	</ul>
</section> 
<!-- end banner -->

<!-- content gallery -->
<section class="gallery-page link-page padding-top-bot gallery-desk-tab">
	<div class="container">
		<div class="col-xs-12">
			<div class="gallery-heading">
				<h1>Moments in Webgurukul</h1>
			</div>
			<div class="gallery-tab">
				<ul class="nav nav-pills">
		    		<li class="active"><a data-toggle="pill" href="#all">All</a></li>
		    		<li><a data-toggle="pill" href="#workshops">Workshops</a></li>
		    		<li><a data-toggle="pill" href="#events">Events</a></li>
		    		<li><a data-toggle="pill" href="#training">Training</a></li>
		    		<li><a data-toggle="pill" href="#expo">Expo's</a></li>
	  			</ul>
			</div>
		</div>
		<div class="col-xs-12 no-padding">
			<div class="tab-content">
				<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'all_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="all" class="tab-pane fade in active">
					<?php  while ( have_posts() ) : the_post(); ?>
					<a href="#" data-toggle="modal" data-target="#all-gallery-slider">
					    <div class="col-md-4 col-sm-6 gallery-img-div">
					    	<div class="text-strip">
					    		<p class="sm"><?php the_content(); ?></p>
					    	</div> 
					    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
					    </div>
				    </a>
				    <?php endwhile; ?>
				</div>

					<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'workshop_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="workshops" class="tab-pane fade">
                  <?php  while ( have_posts() ) : the_post(); ?>
				    <a href="#" data-toggle="modal" data-target="#workshop-gallery-slider">
					    <div class="col-md-4 col-sm-6 gallery-img-div">
					    	<div class="text-strip">
					    		<p class="sm"><?php the_content(); ?></p>
					    	</div>
					    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
					    </div>
				    </a>
				    <?php endwhile; ?>
				</div>

					<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'events_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="events" class="tab-pane fade">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <a href="#" data-toggle="modal" data-target="#event-gallery-slider">
					    <div class="col-md-4 col-sm-6 gallery-img-div">
					    	<div class="text-strip">
					    		<p class="sm"><?php the_content(); ?></p>
					    	</div>
					    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
					    </div>
				    </a>
				    <?php endwhile; ?>
				</div>

					<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'training_gallary'      
                    );
                    query_posts( $args );
                  ?>
				<div id="training" class="tab-pane fade">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <a href="#" data-toggle="modal" data-target="#training-gallery-slider">
					    <div class="col-md-4 col-sm-6 gallery-img-div">
					    	<div class="text-strip">
					    		<p class="sm"><?php the_content(); ?></p>
					    	</div>
					    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
					    </div>
				    </a>
				    <?php endwhile; ?>
				</div>
				
					<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'expo_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="expo" class="tab-pane fade">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <a href="#" data-toggle="modal" data-target="#expo-gallery-slider">
					    <div class="col-md-4 col-sm-6 gallery-img-div">
					    	<div class="text-strip">
					    		<p class="sm"><?php the_content(); ?></p>
					    	</div>
					    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
					    </div>
				    </a>
				    <?php endwhile; ?>
				</div> <!-- end tab pan -->
		    </div> <!-- tab content -->
		</div>
	</div>
</section>

<!--gallery below 720 -->
<section class="gallery-page link-page padding-top-bot gallery-xs">
	<div class="container">
		<div class="col-xs-12">
			<div class="gallery-heading">
				<h1>Moments in Webgurukul</h1>
			</div>
			<div class="gallery-tab">
				<ul class="nav nav-pills">
		    		<li class="active"><a data-toggle="pill" href="#all2">All</a></li>
		    		<li><a data-toggle="pill" href="#workshop2">Workshops</a></li>
		    		<li><a data-toggle="pill" href="#events2">Events</a></li>
		    		<li><a data-toggle="pill" href="#training2">Training</a></li>
		    		<li><a data-toggle="pill" href="#expo2">Expo's</a></li>
	  			</ul>
			</div>
		</div>
		<div class="col-xs-12 no-padding">
			<div class="tab-content">
				<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'all_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="all2" class="tab-pane fade in active">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <div class="col-md-4 col-sm-6 gallery-img-div"> 
				    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
				    	<p><?php the_content(); ?></p>
				    </div>
				    <?php endwhile; ?>
				</div>

				<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'workshop_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="workshop2" class="tab-pane fade in">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <div class="col-md-4 col-sm-6 gallery-img-div">
				    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
				    	<p><?php the_content(); ?></p>
				    </div>
				    <?php endwhile; ?>
				</div>

				<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'events_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="events2" class="tab-pane fade in">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <div class="col-md-4 col-sm-6 gallery-img-div">
				    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
				    	<p><?php the_content(); ?></p>
				    </div>
				    <?php endwhile; ?>
				</div>

				<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'training_gallary'      
                    );
                    query_posts( $args );
                  ?>
				<div id="training2" class="tab-pane fade in">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <div class="col-md-4 col-sm-6 gallery-img-div">
				    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
				    	<p><?php the_content(); ?></p>
				    </div>
				    <?php endwhile; ?>
				</div>
				
				<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'expo_gallery'      
                    );
                    query_posts( $args );
                  ?>
				<div id="expo2" class="tab-pane fade in">
					<?php  while ( have_posts() ) : the_post(); ?>
				    <div class="col-md-4 col-sm-6 gallery-img-div">
				    	<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
				    	<p><?php the_content(); ?></p>
				    </div>
				    <?php endwhile; ?>
				</div> <!-- end tab pan -->
		    </div> <!-- tab content -->
		</div>
	</div>
</section>
<!-- gallery page modal -->

<!-- all-gallery Modal-->
<div class="modal fade gallery-modal-lg" id="all-gallery-slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-md gallery-modal">
  		<button type="button" class="close gallery-close" data-dismiss="modal">&times;</button>
      		<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'all_gallery'      
                    );
                    query_posts( $args );
                  ?>
    	<div class="modal-content">
      		<div id="gallery-modal0" class="carousel slide" data-ride="carousel">
	  			<!-- Wrapper for slides -->
	  			<div class="carousel-inner">
	  				<?php $i=1; while ( have_posts() ) : the_post(); ?>
	    			<div class="item <?php if($i==1){echo 'active';}?>">
	     				<img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="...">
	    			</div>
	    			<?php $i++; endwhile; ?> 
	  			</div>

  				<!-- Controls -->
	  			<a class="left carousel-control" href="#gallery-modal0" role="button" data-slide="prev">
	    			<span class="wkl-left-arrow"></span>
	  			</a>
	  			<a class="right carousel-control" href="#gallery-modal0" role="button" data-slide="next">
	    			<span class="wkl-right-arrow"></span>
	  			</a>
			</div>
    	</div>
  	</div>
</div>

<!-- workshop-gallery Modal-->
<div class="modal fade gallery-modal-lg" id="workshop-gallery-slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-md gallery-modal">
  		<button type="button" class="close gallery-close" data-dismiss="modal">&times;</button>
      		<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'workshop_gallery'      
                    );
                    query_posts( $args );
                  ?>
    	<div class="modal-content">
      		<div id="gallery-modal1" class="carousel slide" data-ride="carousel">
	  			<!-- Wrapper for slides -->
	  			<div class="carousel-inner">
	  				<?php $i=1; while ( have_posts() ) : the_post(); ?>
	    			<div class="item <?php if($i==1){echo 'active';}?>">
	     				<img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="...">
	    			</div>
	    			<?php $i++; endwhile; ?> 
	  			</div>

  				<!-- Controls -->
	  			<a class="left carousel-control" href="#gallery-modal1" role="button" data-slide="prev">
	    			<span class="wkl-left-arrow"></span>
	  			</a>
	  			<a class="right carousel-control" href="#gallery-modal1" role="button" data-slide="next">
	    			<span class="wkl-right-arrow"></span>
	  			</a>
			</div>
    	</div>
  	</div>
</div>

<!-- event-gallery Modal-->
<div class="modal fade gallery-modal-lg" id="event-gallery-slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-md gallery-modal">
  		<button type="button" class="close gallery-close" data-dismiss="modal">&times;</button>
      		<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'events_gallery'      
                    );
                    query_posts( $args );
                  ?>
    	<div class="modal-content">
      		<div id="gallery-modal2" class="carousel slide" data-ride="carousel">
	  			<!-- Wrapper for slides -->
	  			<div class="carousel-inner">
	  				<?php $i=1; while ( have_posts() ) : the_post(); ?>
	    			<div class="item <?php if($i==1){echo 'active';}?>">
	     				<img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="...">
	    			</div>
	    			<?php $i++; endwhile; ?> 
	  			</div>

  				<!-- Controls -->
	  			<a class="left carousel-control" href="#gallery-modal2" role="button" data-slide="prev">
	    			<span class="wkl-left-arrow"></span>
	  			</a>
	  			<a class="right carousel-control" href="#gallery-modal2" role="button" data-slide="next">
	    			<span class="wkl-right-arrow"></span>
	  			</a>
			</div>
    	</div>
  	</div>
</div>

<!-- training-gallery Modal-->
<div class="modal fade gallery-modal-lg" id="training-gallery-slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-md gallery-modal">
  		<button type="button" class="close gallery-close" data-dismiss="modal">&times;</button>
      		<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'training_gallary'      
                    );
                    query_posts( $args );
                  ?>
    	<div class="modal-content">
      		<div id="gallery-modal3" class="carousel slide" data-ride="carousel">
	  			<!-- Wrapper for slides -->
	  			<div class="carousel-inner">
	  				<?php $i=1; while ( have_posts() ) : the_post(); ?>
	    			<div class="item <?php if($i==1){echo 'active';}?>">
	     				<img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="...">
	    			</div>
	    			<?php $i++; endwhile; ?> 
	  			</div>

  				<!-- Controls -->
	  			<a class="left carousel-control" href="#gallery-modal3" role="button" data-slide="prev">
	    			<span class="wkl-left-arrow"></span>
	  			</a>
	  			<a class="right carousel-control" href="#gallery-modal3" role="button" data-slide="next">
	    			<span class="wkl-right-arrow"></span>
	  			</a>
			</div>
    	</div>
  	</div>
</div>

<!-- expo-gallery Modal-->
<div class="modal fade gallery-modal-lg" id="expo-gallery-slider" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-md gallery-modal">
  		<button type="button" class="close gallery-close" data-dismiss="modal">&times;</button>
      		<?php               
                    $args = array(
                        'post_type'=> 'galleries',
                        'category_name' => 'expo_gallery'      
                    );
                    query_posts( $args );
                  ?>
    	<div class="modal-content">
      		<div id="gallery-modal4" class="carousel slide" data-ride="carousel">
	  			<!-- Wrapper for slides -->
	  			<div class="carousel-inner">
	  				<?php $i=1; while ( have_posts() ) : the_post(); ?>
	    			<div class="item <?php if($i==1){echo 'active';}?>">
	     				<img class="img-responsive" src="<?php the_post_thumbnail_url(); ?>" alt="...">
	    			</div>
	    			<?php $i++; endwhile; ?> 
	  			</div>

  				<!-- Controls -->
	  			<a class="left carousel-control" href="#gallery-modal4" role="button" data-slide="prev">
	    			<span class="wkl-left-arrow"></span>
	  			</a>
	  			<a class="right carousel-control" href="#gallery-modal4" role="button" data-slide="next">
	    			<span class="wkl-right-arrow"></span>
	  			</a>
			</div>
    	</div>
  	</div>
</div>

<?php get_footer(); ?>