<?php get_header(); ?>

<!-- banner -->
<section class="blog-detls-banner link-page-banner bg-image">
	<h2 class="banner-heading"></h2>
	<ul class="breadcrumb hidden-xs">
		<li><a href="<?php echo get_site_url(); ?>" class="sm">Home</a></li>
		<li><a href="<?php echo do_shortcode('blog') ?>" class="sm">Blog</a></li>
		<li class="active sm"><?php the_title(); ?></li>
	</ul>
</section>
<!-- end banner -->

<section class="blog-details">
	<div class="container">
		<div class="col-md-8 col-xs-12 blog-container padding-left0">
			<!-- bolg-1 -->
			<?php  while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

			<div class="col-xs-12 blog-page-div no-padding">
				<div class="row blog-div-row1">
					<div class="col-md-9 col-xs-12">
						<h1 class="blog-page-subheading"><?php the_title(); ?></h1>
					</div>
					<div class="col-md-3">
						<p class="blog-page-date lg1"><?php $my_date = the_date('M j, Y', '<span>', '</span>', FALSE); echo $my_date; ?></p>
					</div>
				</div>
				<div class="row blog-div-row2">
					<img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>" alt="blog-image" />
				</div>
				<div class="row blog-div-row3">
					
					 <?php the_content(); ?>
					
					<div class="pull-right">
					 	<p class="blog-name"><?php the_author(); ?></p>
					 	<p class="pull-right">Webgurukul</p>
					</div>
				</div>
			</div>
				<?php endwhile; ?>
			<div class="col-xs-12 blog-page-div no-padding">
				<div class="row blog-div-row1">
					<div id="disqus_thread"></div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 blog-aside">
			<div class="blog-aside-div blog-page-div clearfix">
				<p class="lg1">Recent Posts</p>
				<?php
                        $args = array( 'numberposts' => 2, 'order'=> 'ASC', 'orderby' => 'title' );
                        $postslist = get_posts( $args );
                        foreach ($postslist as $post) :  setup_postdata($post); ?>
				<div class="blog-link">
					<div class="col-xs-1 no-padding"><span class="wkl-paper"></span></div>
					<div class="col-xs-11 padding-left0"><p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p></div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 blog-aside">
			<div class="blog-aside-div blog-page-div clearfix">
				<p class="lg1">Popular Posts</p>
				<div class="col-xs-12 pop-post padding-left0">
					<a href="<?php the_permalink(); ?>"><?php echo do_shortcode('[tptn_list]'); ?></a>
				</div>
			</div> <!-- end blog-aside-div -->
		</div> <!-- end blog-aside -->
	</div>
</section>

<?php get_footer(); ?>
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT 
     *  THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR 
     *  PLATFORM OR CMS.
     *  
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: 
     *  https://disqus.com/admin/universalcode/#configuration-variables
     */
    /*
    var disqus_config = function () {
        // Replace PAGE_URL with your page's canonical URL variable
        this.page.url = PAGE_URL;  
        
        // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        this.page.identifier = PAGE_IDENTIFIER; 
    };
    */
    
    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
        var d = document, s = d.createElement('script');
        
        // IMPORTANT: Replace EXAMPLE with your forum shortname!
        s.src = 'https://EXAMPLE.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>
    Please enable JavaScript to view the 
    <a href="https://disqus.com/?ref_noscript" rel="nofollow">
        comments powered by Disqus.
    </a>
</noscript>