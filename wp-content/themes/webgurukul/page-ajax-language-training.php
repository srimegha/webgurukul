<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Ajax</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Ajax Training In Nagpur</h1>
				<p>Ajax stands for asynchronous JavaScript and XML. It is a group of interrelated web development techniques used on the client-side to create interactive web applications. Ajax is about updating parts of a web page, without reloading the whole page. Ajax uses a combination of HTML5 and CSS3 to mark up style information. Asynchronous JavaScript and XML (AJAX). It’s a combines a set of known technologies in order to create faster and more user-friendly web pages. With the Help of Ajax, it changes to content without refreshing the whole page.</p>
				<p>Ajax Training is an advanced course, before learning this you should have knowledge of PHP, Jquery, JavaScript Language. These technologies are the base of Ajax Technology.</p>
				
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>Graduate and Undergraduate</span></li>
					<li><span>Freshers</span></li>
					<li><span>Professionals</span></li>
					<li><span>Job Seekers</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>PHP </span></li>
					<li><span>jQuery</span></li>
					<li><span>Java</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Ajax Modules</h3>
				<p>This course covers JavaScript and the fundamental code syntax. We show you the DOM and why it is essential for dynamic web content. Then jquery in order to save time and speed up the design process. JSON to transfer data between the front and back-end code. Then PHP constructor code to interact with MySQL to bring data in from a web form. No page refresh with AJAX form submission. Also use AJAX to pull data from PHP in JSON format to output it on your webpage. This is a course that brings it all together how it work together. An AJAX web form can be then used in many variations in order to build out your own dynamic AJAX pages. We also use bootstrap for styling. Learn how to output PHP array data as JSON so that it can be brought into JavaScript to placed within HTML.
			</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Ajax Basics<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The Purpose of Ajax</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The XMLHttpRequest Object</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The Callback Function</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Passing Data<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>XML</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Dynamic Tables</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JSON</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Review of Object Literals</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Arrays</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Back to JSON</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JSON Parsers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JSON Advantages and Disadvantages</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Ajax Applications<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Login Form</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Ajax Slideshow</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: XSLT Transformations with JavaScript<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>XSLT</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic XSLT</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>XSLT in the Browser</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The Mozilla Method</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The IE Method/span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Advantages and Disadvantages of XSLT in Ajax Applications</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Ajax/JavaScript Frameworks<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The Purpose of Frameworks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Choosing a Framework</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Prototype</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: More Ajax Applications<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Inline Editing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The content editable Attribute</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Detailed Information on Demand</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Autologout</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand the basics of Ajax interactions</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Understand and use the XMLHttpRequest() object in Javascript</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Update the Browser Window's HTML content dynamically through the DOM</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Dynamically Create and Send Parameterized Queries to a Server</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Monitor Server Response for Process Completion</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Display Server Response in Plain Text</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Receive and Process XML Objects in the DOM</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Receive and Parse JSON Objects in the DOM</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>To Understand the Role of a Web Server in Ajax</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-ajax course-icon"></span>
                </div>
                <P 	class="xlg">Ajax</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-ajax course-icon"></span>
            </div>
            <P 	class="xlg">Ajax</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
		<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>