<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Bootstrap</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Bootstrap Training In Nagpur</h1>
				<p>Bootstrap is the most popular HTML5, CSS3, and JavaScript framework for developing responsive, mobile-first websites. A technique of loading a program into a computer by means of a few initial instructions which enable the introduction of the rest of the program from an input device.</p>
				<p>Bootstrap Training is important because Bootstrap helps to make Best Responsive Web Design. Bootstrap is a combination of HTML5, CSS3, jQuery, Javascript Framework. Bootstrap is responsive, mobile-first, prevailing, and front-end framework, which is developed along with CSS, JavaScript, and HTML5. Bootstrap has many benefits from scratch for every web development project, and one such reason is the huge number of resources accessible for Bootstrap. Bootstrap course is a 100% JOB oriented Course. On this technology having so many openings in Nagpur IT Park and All Over India. All companies offering good Salary to freshers also.
				</p>
				<p>
				Webgurukul IT Training Institute trained 3000+ Students in Nagpur and Wardha. All our students placed in Reputed IT Companies and In Organization all over India. We have an expert faculty for Bootstrap Training In Nagpur, there are So many coaching they doing business only for money. But webgurukul Don't, We will train you like Expert, After joining this Course what you will See?, You will see you become a professional in Bootstrap Technology. JOIN Webgurukul TODAY.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>Graduate and Undergraduate</span></li>
					<li><span>Fresher’s</span></li>
					<li><span>Professionals</span></li>
					<li><span>Job Seekers</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic Knowledge of HTML5, CSS3</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Bootstrap Modules</h3>
				<p>Bootstrap is an open source project originally created by Twitter to enable the creation of responsive, mobile first web pages. Bootstrap has a standard set of classes that allow developers to quickly create applications that scale to devices of all sizes and incorporate common components such as dialog boxes and validation. Bootstrap has become a de facto standard for web design.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Customize Bootstrap</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Comprehend the Carousel</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Comprehend the Typeahead</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Comprehend the Model</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Comprehend the Bootstrap File Structure</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Comprehend the Default Grid System</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Comprehend the Fluid Grid System</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Comprehend the Responsive Design</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Comprehend the Dropdown Menus</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Comprehend the Button Groups</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: Comprehend the Navbar, Breadcrumbs, Pagination, Labels, and Badges</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: Comprehend the Typographic Elements, Thumbnails, Alerts and Progress Bars</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 13: Comprehend the Media Object, Typography, and Tables</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 14: Comprehend the Forms</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 15: Comprehend the Button</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 16: Comprehend the Images</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 17: Comprehend the Icons</a>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to implement Bootstrap to any project</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Start using Bootstrap into any project</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to use Bootstrap to quickly create responsive layouts</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to use Javascript easily using Bootstrap</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to implement all the Bootstrap styles</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to use the GRID system (important)</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>How to customize Bootstrap into small abstractions of codes (Plugins)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-Boostrap_logo course-icon3"><span class="path1"></span><span class="path2"></span></span>
                </div>
                <P 	class="xlg">Bootstrap</P>
                <p class="text-center"> 
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-Boostrap_logo course-icon3"><span class="path1"></span><span class="path2"></span></span>
            </div>
            <P 	class="xlg">Bootstrap</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->

<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>