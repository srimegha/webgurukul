<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">My SQL</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>MySQL Training In Nagpur</h1>
				<p>WebGurukul Nagpur provides MySQL and DBA classes in Nagpur. Our MySQL dba course includes SQL query related full knowledge and our MySQL course are designed to get the good Job opportunities in companies in Nagpur, Pune as once you complete the PHP MySQL training course. Our MySQL trainers are experts and having 4+ year experience working professionals with hands-on live projects PHP and MySQL projects knowledge. We have designed our MySQL course content and syllabus based on the industrial requirement to achieve a career as Database Administrator or full stack developer.</p>
				<p>Our MySQL dba training centers having good infrastructure with lab facilities. We also provide MySQL dba job oriented advance internship training path for our students in Nagpur. We have a great placement record with frontend and backend developer. We help to plan students to design database architecture of the projects which meets their practical implementation approach. Our MySQL DBA course fee is value for money. MySQL training in Nagpur conducted with PHP classes which helps students to become Job ready for DBA or Backend developer.</p>
				
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<p class="course-details-headings no-padding">Who can Apply?</p>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<p class="course-details-headings no-padding">Required Knowledge</p>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of HTML, CSS, PHP.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<p class="course-details-headings">MySQL Modules</p>
				<p>MySQL Database Being the Second most widely used Relational Database makes it one of the database to learn if you are looking forward to develop a database driven application.</p>
				<p>MySQL is one of the most used and go to database for pretty much all the web developer who pick PHP, PEARL or Python without a second thought.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction About MySQLi<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Installation</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>MySQLi Administration</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: 	MySQLi PHP Syntax</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: 	MySQLi PHP Connection</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: 	MySQLi DATABASE<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create Database</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Drop Database</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Select Database</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5:	MySQLi Datatypes</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: 	MySQLi DDL<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Alter</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Drop</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: 	MySQLi DML<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Select</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Insert</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Update</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Delete</span></a>
							</li>
						</ul>
					</li>
						<li>
						<a href="javascript:void(0)" class="lg">Module 8: 	MySQLi Where Clause</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: 	MySQLi Like Clause</a>
					</li>
						<li>
						<a href="javascript:void(0)" class="lg">Module 10: 	MySQLi Sorting</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: 	Handling Null values</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: 	MySQLi Transactions</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 13: 	MySQLi Alter Statements</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 14: 	MySQLi Indexes</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 15: 	MySQLi Temporary & Clone Table</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 16: 	MySQLi Using Sequences</a>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<p class="course-details-headings">What will you are going to Learn?</p>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create a Well Structure Database</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Handle a Database with Pretty good efficiency</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create a Database that can handle itself on most conditions</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create MySQL Queries that are efficient and clear</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand the errors as they occur and rectify them</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Administrate the Database with Ease</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create SQL Queries for Database Driven Applications</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Spot abnormality in Database Table</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Database Structure as needed for good design</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Indexing Tables for Better Performance</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Good practices to be followed</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div"> 
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-sql course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span><span class="path51"></span><span class="path52"></span><span class="path53"></span><span class="path54"></span><span class="path55"></span><span class="path56"></span><span class="path57"></span><span class="path58"></span><span class="path59"></span><span class="path60"></span><span class="path61"></span><span class="path62"></span><span class="path63"></span><span class="path64"></span><span class="path65"></span><span class="path66"></span><span class="path67"></span><span class="path68"></span><span class="path69"></span><span class="path70"></span><span class="path71"></span><span class="path72"></span><span class="path73"></span><span class="path74"></span><span class="path75"></span><span class="path76"></span><span class="path77"></span><span class="path78"></span><span class="path79"></span><span class="path80"></span><span class="path81"></span><span class="path82"></span><span class="path83"></span><span class="path84"></span><span class="path85"></span><span class="path86"></span><span class="path87"></span><span class="path88"></span></span>
                </div>
                <P 	class="xlg">MySQL</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-sql course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span><span class="path51"></span><span class="path52"></span><span class="path53"></span><span class="path54"></span><span class="path55"></span><span class="path56"></span><span class="path57"></span><span class="path58"></span><span class="path59"></span><span class="path60"></span><span class="path61"></span><span class="path62"></span><span class="path63"></span><span class="path64"></span><span class="path65"></span><span class="path66"></span><span class="path67"></span><span class="path68"></span><span class="path69"></span><span class="path70"></span><span class="path71"></span><span class="path72"></span><span class="path73"></span><span class="path74"></span><span class="path75"></span><span class="path76"></span><span class="path77"></span><span class="path78"></span><span class="path79"></span><span class="path80"></span><span class="path81"></span><span class="path82"></span><span class="path83"></span><span class="path84"></span><span class="path85"></span><span class="path86"></span><span class="path87"></span><span class="path88"></span></span>
            </div>
            <P 	class="xlg">MySQL</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
<?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
		<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>