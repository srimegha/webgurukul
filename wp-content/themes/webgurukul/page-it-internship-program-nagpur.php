<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">IT Internship Program</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Join IT Internship Program in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				WebGurukul <strong>IT Internship in Nagpur</strong>, this program is a good start for students who are looking for their career to start in the field of web development and software development. In this internship program  we helps the student to deliver themselves in the industry of <strong>Web development,  E-commerce development</strong> and many relevant skills. Here trainers are working with more than 5+ years of Software Development experts, which help them a lot in Technical knowledge boosting. IT Internship Nagpur  program is a full-time training program for trainees where they work on live projects running under WebGurukul.
			</p>
			<p>
				WebGurukul  is announcing <strong>IT internship, Summer internship, Full semester internship and anytime virtual internship</strong>. Programs for Engineering Students (BE), MCA students, BCA students, BTech Students, This internship training programs for computer science, Information Technology and Other streams of engineering.
			</p>
            <p>
                In this IT internship Nagpur  training program we will cover all technologies related to website development and software development, Technologies are <strong>HTML5, CSS3, JavaScript, JQuery, AJAX, PHP, MySQL, WordPress and related frameworks.</strong>
            </p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-employee course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-InternshipProgram.pdf" target="_blank" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot light-grey internship-section">
    <div class="container">
        <div class="row">
            <h2 class="text-center">Most Preferred Internship Plans</h2>
            <p class="sec-subheading text-center">Select your plans according to your career planning.</p>
            <div class="course-row no-padding clearfix long-term-module custom-flex">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="internship-module-div col-xs-12">
                        <h3 class="xlg"><b>180 Days Program</b></h3>
                        <div class="module-details">
                            <p class="sm">Syllabus :</p>
                            <ul class="list-unstyled no-padding check-list">
                                <li>Basics of Web Development.</li>
                                <li>Adv. Website Template Development.</li>
                                <li>Backend Programming & Mini Projects. </li>
                                <li>Adv. Frameworks. </li>
                                <li>Full Time 90 Days Industrial Exposure. </li>
                                <li>Live Project Experience and Job Assistance. </li>
                            </ul>
                            <div class="btn-section clearfix ">
                                <a href="<?php echo get_site_url(); ?>/registration" class="btn btn-line-orng">Register</a>
                                <a href="<?php echo get_site_url(); ?>/job-oriented-internship" class="btn btn-line-black">Details</a>       
                            </div>  
                        </div>
                    </div>
                    <span class="blue-badge">JOB ORIENTED</span>                
                </div>
                <div class="col-md-1 hidden-sm" style="width:30px"></div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="internship-module-div col-xs-12">
                        <h3 class="xlg"><b>90 Days Program</b></h3>
                        <div class="module-details">
                            <p class="sm">Syllabus :</p>
                            <ul class="list-unstyled no-padding check-list">
                                <li>Basics of Web Development. </li>
                                <li>Adv. Website Template Development. </li>
                                <li>Template Practice. </li>
                                <li>Backend Programming. </li>
                                <li>Mini Projects. </li>
                                <li>No Job Assistance. </li>
                            </ul>
                            <div class="btn-section clearfix ">
                                <a href="<?php echo get_site_url(); ?>/registration" class="btn btn-line-orng">Register</a>
                                <a href="<?php echo get_site_url(); ?>/skill-based-internship" class="btn btn-line-black">Details</a>       
                            </div>  
                        </div>
                    </div>
                    <span class="blue-badge">SKILLS DEVELOP</span>             
                </div>      
            </div>            
        </div>

        <div class="row padding-top-bot">
            <h2 class="text-center">Short Term Internship Plans</h2>
            <div class="course-row no-padding clearfix short-term-module custom-flex">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="internship-module-div clr-green col-xs-12">
                        <h3 class="xlg"><b>45 Days Program</b></h3>
                        <div class="module-details">
                            <p class="sm">Syllabus :</p>
                            <ul class="list-unstyled no-padding check-list">
                                <li>Basics of Web Designing. </li>
                                <li>Adv. Website Template Development. </li>
                                <li>Basic Backend Programming. </li>
                                <li>Practice Project. </li>
                            </ul>
                            <div class="btn-section clearfix ">
                                <a href="<?php echo get_site_url(); ?>/registration" class="btn btn-line-green">Register</a>
                                <a href="<?php echo get_site_url(); ?>/short-term-advance-internship" class="btn btn-line-black">Details</a>       
                            </div>  
                        </div>
                    </div>                    
                </div>
                <div class="col-sm-1 hidden-xs" style="width:30px"></div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="internship-module-div clr-green col-xs-12">
                        <h3 class="xlg"><b>15 Days Program</b></h3>
                        <div class="module-details">
                            <p class="sm">Syllabus :</p>
                            <ul class="list-unstyled no-padding check-list">
                                <li>Basics of Web Designing. </li>
                                <li>Basic Template Practicing. </li>
                                <li>Fewer Practicals in Class. </li>
                                <li>Introduction to Web Development. </li>
                            </ul>
                            <div class="btn-section clearfix ">
                                <a href="<?php echo get_site_url(); ?>/registration" class="btn btn-line-green">Register</a>
                                <a href="<?php echo get_site_url(); ?>/short-term-basic-internship" class="btn btn-line-black">Details</a>       
                            </div>  
                        </div>
                    </div>                    
                </div>
                
                
            </div>            
        </div>
    </div>
</section>
<!-- end courses-module section -->

<section class="course-module padding-top-bot grey-section internship-section">
    <div class="container">
    <h2 class="text-center sec-heading">Benefits of Internship Program</h2>
    <p class="sec-subheading text-center">Internship Program at WebGurukul is a power-packed technical development program which boosts the programming logic of the student which make them fearless to work with the web development or software development industry. Joining Internship make a drastic change in students technical profiles, this is already proven by the pass out students from WebGurukul. Here is an internship program, students not only learn about Web development skills but also the corporate ethics, team leading skills those are the most preferred in any IT corporate industry.</p>
    <div class="course-row no-padding clearfix">
        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-htmlncss2 course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>HTML5 &amp; CSS3</h3>
                <a href="<?php echo get_site_url(); ?>/html-css-language" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-Adobe_Photoshop course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>Photoshop</h3>
                <a href="<?php echo get_site_url(); ?>/photoshop-design" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-Boostrap_logo course-icon"><span class="path1"></span><span class="path2"></span></span>
                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>BootStrap</h3>
                <a href="<?php echo get_site_url(); ?>/bootstrap-technology" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-JSnJquery course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></span>
                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>JavaScript &amp; jQuery</h3>
                <a href="<?php echo get_site_url(); ?>/javascript-jquery-training" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>
    </div>

    <div class="course-row no-padding clearfix">
        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-php course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span>
                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>PHP</h3>
                <a href="<?php echo get_site_url(); ?>/php-training" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-sql course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span><span class="path51"></span><span class="path52"></span><span class="path53"></span><span class="path54"></span><span class="path55"></span><span class="path56"></span><span class="path57"></span><span class="path58"></span><span class="path59"></span><span class="path60"></span><span class="path61"></span><span class="path62"></span><span class="path63"></span><span class="path64"></span><span class="path65"></span><span class="path66"></span><span class="path67"></span><span class="path68"></span><span class="path69"></span><span class="path70"></span><span class="path71"></span><span class="path72"></span><span class="path73"></span><span class="path74"></span><span class="path75"></span><span class="path76"></span><span class="path77"></span><span class="path78"></span><span class="path79"></span><span class="path80"></span><span class="path81"></span><span class="path82"></span><span class="path83"></span><span class="path84"></span><span class="path85"></span><span class="path86"></span><span class="path87"></span><span class="path88"></span></span>


                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>MySQL</h3>
                <a href="<?php echo get_site_url(); ?>/my-sql-training" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-ajax course-icon"></span>
                <span class="course-star"><span class="wkl-star"></span></span>
                </div>
                <!-- subheading -->
                <h3>Ajax</h3>
                <a href="<?php echo get_site_url(); ?>/ajax-language-training" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="course-card col-xs-12">
                <!-- icon-circle -->
                <div class="course-circle">
                    <span class="wkl-wordpress course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                <span class="course-star clr-green"><span class="wkl-star"></span></span>
                </div>
                <span class="new-strip xs">New</span>
                <!-- subheading -->
                <h3>WordPress</h3>
                <a href="<?php echo get_site_url(); ?>/wordpress-design" class="">More <span class="dir-arrow"> >> </span></a>
            </div>
        </div>
    </div>
        
    </div>
</section>



<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Internship Program</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a experienced programmer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="<?php echo get_site_url(); ?>/contact" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!-- end course page register and contact section -->


<?php get_footer(); ?>