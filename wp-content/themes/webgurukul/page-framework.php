<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">CMS &amp; Framework</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn CMS &amp; Framework Course in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				There are many Web Designing Courses in Nagpur. But Webgurukul is only the best destination webgurukul is a best IT Training institute in Nagpur that teaches you from very basic in Web Designing Course practically. Here we make you master in Web Designing by providing excellent coaching with live practical orientation. The advance Web Design Course is for those who are looking bright career in IT Sector, Webgurukul is the best place to make your bright career in IT Field.
			</p>
			<p>
				If you are passionate about web designing Webgurukul is a perfect place. We will provide perfect training and Guidance, this Web Design Course in Nagpur and Wardha. Join Today Best IT Training Institute Webgurukul and being a master in Web Designing.
			</p>
			<p>
				Webgurukul is a Leading IT Training Institute in Nagpur, We offers Job oriented advanced Web Design Course in this course you will teach how to design Responsive and Attractive website. Web Designing course content and syllabus based on students requirement to achieve everyone's career goal. If you are thinking career in IT Sector, Join job oriented Web Design Course at Webgurukul. Our Team will make you Best Coder in Web Design. 
			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-employee course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-FrameworkCourses.pdf" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot grey-section">
	<div class="container">
		<h2 class="text-center">Get your CMS &amp; Framework Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class="row course-row no-padding">
            <div class="col-md-4 col-sm-6 col-xs-12 margin-right-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-cakephp course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Cake PHP</h3>
                    <!-- para -->
                    <p>CakePHP makes building web applications simpler, faster, while requiring less code. A modern PHP 7 framework offering a flexible database access layer and a powerful scaffolding system that makes building both small and complex systems simpler and easier.</p>
                    <a href="<?php echo get_site_url(); ?>/cake-php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    
                    <div class="course-circle">
                    	<span class="wkl-wordpress course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                    </div>
                    
                    <h3>WordPress</h3>
                    
                    <p>WordPress is a free and open-source content management system (CMS) based on PHP and MySQL. To function, WordPress has to be installed on a web server, which would either be part of an Internet hosting service or a network host in its own right.</p>
                    <a href="<?php echo get_site_url(); ?>/wordpress-design" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div> 
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    
                    <div class="course-circle">
                    	<span class="wkl-laravel-logo course-icon"></span>
                    </div>
                    
                    <h3>Laravel</h3>
                    
                    <p>WordPress is a free and open-source content management system (CMS) based on PHP and MySQL. To function, WordPress has to be installed on a web server, which would either be part of an Internet hosting service or a network host in its own right.</p>
                    <a href="<?php echo get_site_url(); ?>/laravel" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div> 
            </div>
        </div>          
	</div>
</section>
<!-- end courses-module section -->




<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join CMS &amp; Framework Course</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Developer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="<?php echo get_site_url(); ?>/contact" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!-- end course page register and contact section -->



<?php get_footer(); ?>