<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<p class="banner-heading">WordPress</p>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>WordPress Training In Nagpur</h1>
				<p>Webgurukul is an Official partner of WordPress Community Nagpur our teaching faculty is an Certified and Official Member of WordPress Organization of Nagpur, Webgurukul is a First WordPress Training Center in Nagpur ,we offered only JOB oriented Advance IT courses. We trained 3000+ Students in Nagpur and Wardha. Our second branch is in Wardha</p>
				<p>In Nagpur there are so many IT Training Institute they "ONLY TEACH" But webgurukul make you  "EXPERT , MASTER, PROFFESIONAL" in WordPress and other IT Courses. We provide Internship Program to our trainees. Same training we Provide IT Training in Wardha also.</p>
				<p>All IT Companies have a Huge Demand for WordPress Developer, All IT Companies offering Good Packages for fresher's and Professionals . Don't waste your time and join WordPress Training in Webgurukul.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>Bloggers, Story writers</span></li>
					<li><span>Web Designing Beginners</span></li>
					<li><span>Web developers</span></li>
					<li><span>Who want to build their own websites</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of HTML, CSS.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">WordPress Beginner Modules</h3>
				<p>We think learning of WordPress is very easy, if you are learning with baby steps. As per our experience of learning WordPress, we have designed some modules of learning which covers both the roles if you want to be beginner with WordPress or you want to be WordPress Developer. </p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Getting Started with WordPress<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of WordPress</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>History of WordPress</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why WordPress is preferred for Website Development?</span></a></li>
							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: How to setup WordPress<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Technical Pre-requisites before installing WordPress  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Step by step WordPress Setup</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Importance of every steps</span></a></li>						
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Introduction to WordPress Dashboard <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is the difference between Page &amp; Post?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Categories?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to Create Post &amp; Pages?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to create and assign Categories to the Posts?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to Create Menu in WordPress to link the pages?</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Introduction about themes &amp; Plugins in WordPress <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How to install Themes &amp; their settings</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is plugin &amp; their installations</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Getting started with plugin integration</span></a></li>
						</ul>
					</li>			
				</ul>
			</div>
			<div class="course-module-list course-details-block col-xs-12">
				<p class="course-details-headings">WordPress Advance Modules</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Folder and file structure in WordPress<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Core folder structure of WordPress</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Importance of "wp-content" Folder</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Importance of Upload, theme, plugin folders</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Theme development in WordPress<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Core file structure of WordPress theme</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Importance of each files in theme development</span></a></li>			
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Plugin development in WordPress <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Core file structure of WordPress Plugin</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Importance of each files in Plugin development</span></a></li>
						</ul>
					</li>					
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>You can make your own website with WordPress</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>You can make your blogging or story writing website</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Can be get beginner level Job at any Website Development Company</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>You can make your Career as a WordPress Developer.</p>
						</div>
					</div>
				</div>			
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-wordpress course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                </div>
                <p 	class="xlg">WordPress</p>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-FrameworkCourses.pdf" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-wordpress course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            </div>
            <P 	class="xlg">WordPress</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-FrameworkCourses.pdf" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
<?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>