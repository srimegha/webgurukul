<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Graphics Designing Classes</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn Graphics Designing Course in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				WebGurukul's Graphics Designing course is a comprehensive course that enables you to become a consummate graphics design professional with strong aesthetic skills and the ability to design across multiple mediums for a variety of purposes. This course will teach you the fundamental principles of <strong>graphics design</strong>: image making, typography, composition, working with color and shape... foundational skills that are common in all areas of graphic design practice.


			</p>
			<p>
				WebGukurul not only trains you in designing fundamentals but also gives a chance to work on practical applications such as identity design (logos and icons), communication design, poster design, infographics and designing for merchandise.</p>

			<p>
				If you are thinking to become UI/UX Designer or <strong>Graphics Designer</strong>, then WebGurukul is the best place in Nagpur, Wardha & Amravati. In this course, our experienced designer will train you in all basics and advance concepts of graphics.
			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-web-development-icon course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-GraphicsDesigningCourse.pdf" target="_blank" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!--courses-module section -->
<section class="course-module padding-top-bot grey-section">
	<div class="container">
		<h2 class="text-center">Get your Graphics Designing Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class="row course-row no-padding">
            <div class="col-md-4 col-sm-6 col-xs-12 margin-right-15 col-md-offset-2">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-Adobe_Photoshop course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Advance Photoshop</h3>
                    <!-- para -->
                    <p>If you already know the Photoshop basics or not, our Advance Photoshop training will blow your mind and take you to the next level.</p>
                    <a href="<?php echo get_site_url(); ?>/advance-photoshop" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-illustrator course-icon"><span class="path1"></span><span class="path2"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>illustrator</h3>
                    <!-- para -->
                    <p>Learn to use the world's leading illustration and vector drawing application Illustrator to create artwork for print, for the web.</p>
                    <a href="<?php echo get_site_url(); ?>/illustrator" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>     
	</div>
</section>
<!-- end courses-module section -->



<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Graphics Designing Course</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Graphics Designer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="<?php echo get_site_url(); ?>/contact" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!-- end course page register and contact section -->



<?php get_footer(); ?>