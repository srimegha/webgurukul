<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webgurukul
 */

get_header(); ?>

    
    <div class="container-fluid no-margin no-padding">
        <!-- top-slider -->
        <?php  $args = array('post_type'=> 'slider');
            query_posts( $args );
        ?>
        <div class="top-slider">
            <div id="top-slider" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
              <?php $i=1; while ( have_posts() ) : the_post(); ?>
                    <div class="item s-bg1 bg-image <?php if($i==1){echo 'active';}?>">
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <div class="carousel-caption slider-div col-lg-5 col-sm-6 col-xs-7">
                            <h1 class="slider-heading"><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                            <a href="<?php echo get_page_link(); ?>" class="btn btn-line-orng">Start Learning Now</a>
                        </div>
                    </div>
                    <?php $i++; endwhile; ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#top-slider" role="button" data-slide="prev">
                    <span class="wkl-left-arrow slider-arrow hidden-xs" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#top-slider" role="button" data-slide="next">
                    <span class="wkl-right-arrow slider-arrow hidden-xs" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div><!-- end top slider -->


    <!-- section-1 -->
    <section class="training-company padding-top-bot">
        <div class="container">
            <h1 class="sec-heading text-center">Webgurukul best training company in Nagpur</h1>
            <div class="col-xs-12 section1-para">
                <p class="text-center">WebGurukul is a Leading IT Training Institute in Nagpur. It is also located in Wardha. We are building the students' technical knowledge &amp; training them on different Web Development Courses and Web Designing Courses, in coordination with that we are building their confidence to stand them in a competitive world. We are bridging the gap between students &amp; professional industries. Our Education programs provide recreational and innovative techniques for Web Designing Course and Web Development Course, Mobile App Designing and Development courses, Digital Marketing Course to compose the student from basic to the scholar.</p>
                <p class="text-center sec-para">At WebGurukul, we especially concentrate on the different channels in a necessity of the student to achieve his ultimate goal of getting dream job which is the uttermost need of a youngster. This program is one of the very new initiatives to empower the education of Nagpur &amp; Wardha students at very affordable prices which the student deserves. Here we offer students the expertise training on a different concept of Software Development Industry.</p>
            </div>
            <h2 class="text-center">Courses We Provide</h2>
            <p class="sec-subheading text-center">Our courses which will help you to reach your career goals and to get your dream job.</p>
            <div class="row course-row no-padding">
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="course-box col-xs-12">
                        <!-- icon-circle -->
                        <div class="course-circle">
                            <span class="wkl-web-designing-icon course-icon">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span></span>
                        </div>
                        <!-- subheading -->
                        <h3 class="text-center">Website Designing Classes</h3>
                        <!-- para -->
                        <p>In this course you will learn how to create a user experience design, interactive design and responsive website design.</p>
                        <a href="<?php echo get_site_url(); ?>/web-design" class="btn border-btn1 read-btn">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="course-box col-xs-12">
                        <!-- icon-circle -->
                        <div class="course-circle">
                            <span class="wkl-web-development-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                        </div>
                        <!-- subheading -->
                        <h3 class="text-center">Website Development Classes</h3>
                        <!-- para -->
                        <p>This course contains deep knowledge about Logical working of the Web Apps and building their functionalities from scratch.</p>
                        <a href="<?php echo get_site_url(); ?>/web-development" class="btn border-btn1 read-btn">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="course-box col-xs-12">
                        <!-- icon-circle -->
                        <div class="course-circle">
                            <span class="wkl-employee course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
                        </div>
                        <!-- subheading -->
                        <h3 class="text-center">IT Internship Program</h3>
                        <!-- para -->
                        <p>Work with company experts on live project. This program will provide you the platform to test and build your skills like professionals.</p>
                        <a href="<?php echo get_site_url(); ?>/it-internship-program-nagpur" class="btn border-btn1 read-btn">Read More</a>
                    </div>
                </div>
            </div>
            <div class="row course-row no-padding">
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                   <div class="course-box col-xs-12">
                        <!-- icon-circle -->
                        <div class="course-circle">
                            <span class="wkl-digital-marketing course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></span>
                        </div>
                        <!-- subheading -->
                        <h3 class="text-center">Digital Marketing Course</h3>
                        <!-- para -->
                        <p>This is a leading Digital Marketing Course, in which you will learn A to Z of Digital Marketing from website designing to Marketing tactics.</p>
                        <a href="<?php echo get_site_url(); ?>/digital-marketing-course-in-nagpur" class="btn border-btn1 read-btn">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="course-box col-xs-12">
                        <!-- icon-circle -->
                        <div class="course-circle">
                            <span class="wkl-Graphics-design-course course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                        </div>
                        <!-- subheading -->
                        <h3 class="text-center">Graphics Designing Classes</h3>
                        <!-- para -->
                        <p>Start your career in graphics designing. Learn all concept of designing from scratch with us. Explore the world of creativity.</p>
                        <a href="<?php echo get_site_url(); ?>/graphics-design-classes" class="btn border-btn1 read-btn">Read More</a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="course-box col-xs-12">
                        <!-- icon-circle -->
                        <div class="course-circle">
                            <span class="wkl-workshop course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
                        </div>
                        <!-- subheading -->
                        <h3 class="text-center">Technical Workshop</h3>
                        <!-- para -->
                        <p  class="para-margin-top-on1024">Workshops are helpful for college students to get more practical knowledge in a shorter time and to decide their career path.</p>
                        <a href="<?php echo get_site_url(); ?>/technical-workshop" class="btn border-btn1 read-btn">Read More</a>
                    </div>
                </div>
            </div>      
        </div>
    </section>
    <!-- end section-1 -->

    <!-- start brochure section -->
    <section class="brochure bg-image padding-top-bot">
        <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-12 text-div">
                 <h2>Beginner’s Guide to Web Designing and Development</h2>
                 <p class="no-padding sec-subheading">Our trainers explains the basics of web designing and development and point you in the right direction based on your goals.</p>
                 <a href="<?php echo get_site_url(); ?>/wp-content/uploads/2018/02/WebGurukul-Courses.pdf" target="_blank" class="btn fill-btn2 download-btn">Download Brochure</a>
            </div>
            <div class="col-md-5 col-md-offset-1 col-sm-6 video-div hidden-xs"> 
                <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#videoModal" data-theVideo="https://www.youtube.com/embed/bWPMSSsVdPk">
                    <span class="wkl-youtube-1 youtube-icon"><span class="path1"></span><span class="path2"></span></span>
                </a>
                <p class="video-text lg text-center">Watch a Quick Overview</p>
                <img class="img-responsive imgwidth-100 visible-md visible-lg" src="<?php bloginfo("template_directory"); ?>/img/laptop-img.png">
                <img class="img-responsive imgwidth-100 visible-sm visible-xs" src="<?php bloginfo("template_directory"); ?>/img/laptop1-img.png">
            </div>
        </div>
    </section>
    <!-- end brochure section -->

    <!-- modal for video -->
    <div id="videoModal" class="modal fade video-modal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close video-close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="500" src=""></iframe>
                </div>
            </div>
        </div>
    </div> <!-- end video modal -->
    <!-- end brochure section -->

    <!-- start why to join webgurukul -->
    <section class="why-join padding-top-bot">
        <div class="container">
            <h2 class="col-xs-12">Why to join Webgurukul</h2>
            <!-- heading on desktop -->
            <div class="row no-margin why-join-subheading visible-md visible-lg">
                <div class="col-md-6">
                    <p class="sec-subheading join-heading1">Learn from step-by-step instructions combined.</p>
                </div>
                <!-- <div class="col-md-6">
                    <p class="sec-subheading join-heading2">What do you Get?</p>
                </div> -->
            </div>
            <div class="row">
                <!-- visible on lg, md, xs -->
                <div class="col-md-4 join-points hidden-sm">
                    <!-- heading on mobile -->
                    <p class="sec-subheading text-center hidden-md 
                    hidden-lg">Learn from step-by-step instructions combined.</p>
                    <ul class="sm">
                        <li>
                            <span class="wkl-expert-trainers join-icon glyphicon"></span> Expert Profesional Trainers.
                        </li>
                        <li>
                            <span class="wkl-copmany-certificate join-icon glyphicon"></span> Company Training Certification.
                        </li>
                        <li>
                            <span class="wkl-one-year-membership join-icon glyphicon"></span> 1 Year of Membership.
                        </li>
                        <li>
                            <span class="wkl-live-practical join-icon glyphicon"></span> Live and Practical Projects.
                        </li>
                        <li>
                            <span class="wkl-daily-doubt-solving join-icon glyphicon"></span> Daily doubt Solving Sesion.
                        </li>
                        <li>
                            <span class="wkl-interview-prepare join-icon glyphicon"></span> Interview Preparation.
                        </li>
                        <li>
                            <span class="wkl-personal-attention join-icon glyphicon"></span> Personal Attention.
                        </li>
                    </ul>
                </div>
                <!-- visible in sm -->
                <div class="col-sm-12 join-points visible-sm">
                    <!-- heading on tab -->
                    <p class="sec-subheading text-center hidden-md hidden-lg">Learn from step-by-step instructions combined.</p>
                    <div class="col-sm-6">
                        <ul class="sm">
                            <li>
                                <span class="wkl-expert-trainers join-icon glyphicon"></span> Expert Profesional Trainers.</li>
                            <li>
                                <span class="wkl-copmany-certificate join-icon glyphicon"></span> Company Training Certification.</p>
                            </li>
                            <li>
                                <span class="wkl-one-year-membership join-icon glyphicon"></span> 1 Year of Membership.
                            </li>
                            <li>
                                <span class="wkl-live-practical join-icon glyphicon"></span> Live and Practical Projects.
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="sm">
                            <li>
                                <span class="wkl-daily-doubt-solving join-icon glyphicon"></span> Daily doubt Solving Session.
                            </li>
                            <li>
                                <span class="wkl-interview-prepare join-icon glyphicon"></span> Interview Preparation.
                            </li>
                            <li>
                                <span class="wkl-personal-attention join-icon glyphicon"></span> Personal Attention.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 join-percent">
                    <!-- <p class="sec-subheading text-center hidden-md hidden-lg">What do you Get?</p> -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="percent-div">
                                <div class="percent-circle">
                                    <span class="wkl-practical-training icon-100p"></span>
                                </div>
                                <div class="point-1">
                                    <!-- <p class="percent-p">100 Percent</p> -->
                                    <p class="point-p">Practical Sessions</p>
                                </div>
                                <a href="#">
                                    <span class="wkl-right-arrow join-right-arrow"></span>
                                </a> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="percent-div" data-toggle="modal" data-target="#modal-2">
                                <div class="percent-circle">
                                    <span class="wkl-experience-trainer icon-100p"></span>
                                </div>
                                <div class="point-1">
                                    <!-- <p class="percent-p">10+ Years of</p> -->
                                    <p class="point-p">Experience Trainers
                                    </p>
                                </div>
                                <a href="#">
                                    <span class="wkl-right-arrow join-right-arrow"></span>
                                </a> 
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-6">
                            <div class="percent-div">
                                <div class="percent-circle">
                                    <span class="wkl-live-project-work icon-100p"></span>
                                </div>
                                <div class="point-1">
                                    <!-- <p class="percent-p">100 Percent</p> -->
                                    <p class="point-p">Live Project Work</p>
                                </div>
                                <a href="#">
                                    <span class="wkl-right-arrow join-right-arrow"></span>
                                </a> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="percent-div" data-toggle="modal" data-target="#modal-4">
                                <div class="percent-circle">
                                   <span class="wkl-it-corporate-training icon-100p"></span>
                                </div>
                                <div class="point-1">
                                    <!-- <p class="percent-p">100 Percent</p> -->
                                    <p class="point-p">IT Corporate Training</p>
                                </div>
                                <a href="#">
                                    <span class="wkl-right-arrow join-right-arrow"></span>
                                </a> 
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-6">
                            <div class="percent-div">
                                <div class="percent-circle">
                                    <span class="wkl-placement-assistence icon-100p"></span>
                                </div>
                                <div class="point-1">
                                    <!-- <p class="percent-p">100 Percent</p> -->
                                    <p class="point-p">Placement Assistance</p>
                                </div>
                                <a href="#">
                                    <span class="wkl-right-arrow join-right-arrow"></span>
                                </a> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="percent-div">
                                <div class="percent-circle">
                                    <span class="wkl-free-study-material icon-100p"></span>
                                </div>
                                <div class="point-1">
                                    <!-- <p class="percent-p">100 Percent</p> -->
                                    <p class="point-p">Free study material</p>
                                </div>
                                <a href="#">
                                    <span class="wkl-right-arrow join-right-arrow"></span>
                                </a> 
                            </div> <!-- end percent-div -->
                        </div>
                    </div>
                </div> <!-- end join-percent -->
            </div> 
        </div> 
    </section>

    

    <!-- start counts section -->
    <section class="counts-section padding-top-bot">
        <div class="container">
            <h2 class="text-center">We are collaboratively growing day by day</h2>
            <p class="sec-subheading text-center">Webgurukul has impacted over 1500 students and still count is increasing.</p>
            <div class="row count-row text-center">
                <div class="col-lg-3 col-sm-6 col-xs-6 count-div">
                    <p>
                        <span class="timer count-title count-number counts" data-to="150" data-speed="5000"></span> 
                        <span class="counts">+</span><span class="count-text">&nbsp;&nbsp;Learners</span>
                    </p>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-6 count-div">
                    <p>
                        <span class="timer count-title count-number counts" data-to="30" data-speed="5000"></span> 
                        <span class="counts">+</span><span class="count-text">&nbsp;&nbsp;Placements</span>
                    </p>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-6 count-div">
                    <p>
                        <span class="timer count-title count-number counts" data-to="1700" data-speed="5000"></span> 
                        <span class="counts">+</span><span class="count-text">&nbsp;&nbsp;Alumnies</span>
                    </p>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-6 count-div">
                    <p>
                        <span class="timer count-title count-number counts" data-to="20" data-speed="5000"></span> 
                        <span class="counts">+</span><span class="count-text">&nbsp;&nbsp;Courses</span>
                    </p>
                </div>
            </div> <!-- end count row -->
        </div>
    </section>
    <!-- end counts section -->

    <!--start testimonial-slider -->
    <?php  $args = array('post_type'=> 'students_testimonial');
    query_posts( $args );
    ?>
    <section class="testimonial padding-top-bot clearfix">
        <h2 class="text-center">What Students are Saying</h2>
        <p class="sec-subheading text-center">Join <strong>1500+ </strong>satisfied student in Webgurukul.</p>
            <div class="col-xs-12">
                <div class="testimonial-slider text-center" 
                id="testimonial-slider">
            <?php  while ( have_posts() ) : the_post(); ?>
                    <div class="col-md-6 col-xs-12">
                        <div class="t-slider-div">
                            <p class="text-center">
                                <span class="wkl-left-quote quote-icon">
                                </span>
                            </p> 
                            <p><?php the_content(); ?></p>
                            <div class="arrow-div"></div>
                        </div> 
                        
                        <img class="img-responsive student-photo" src="<?php the_post_thumbnail_url(); ?>" alt="student-photo" />
                        <p class="name"><?php the_title(); ?></p>
                        <?php if( get_field('student_batch') ): ?>
                        <p class="batch"><?php the_field('student_batch'); ?></p>
                        <?php endif; ?>
                    </div>
            <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
    <!--end testimonial-slider -->
    <!-- start blog-section -->
    <!-- <section class="blog-section grey-section padding-top-bot">
        <div class="container">
            <h2 class="text-center">Our Blog</h2>
            <p class="sec-subheading text-center">We are very creative blogers. You can learn many things from it.</p>
            <div class="row">
               <?php
                $temp = $wp_query; $wp_query= null;
                $wp_query = new WP_Query(); $wp_query->query('showposts=3' . '&paged='.$paged); ?>
                <?php  while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        
                <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="blog-box col-xs-12">
                        <div class="col-xs-12 no-padding">
                            
                            <img class="img-responsive imgwidth-100" src="<?php the_post_thumbnail_url(); ?>">
                            
                            <p class="blog-heading"><?php the_title(); ?></p>
                            
                            <p class="blog-text-small">
                                <span class="wkl-calendar-page-empty"></span>&nbsp; <?php $my_date = the_date('M j, Y', '<span class="post-date">', '</span>', FALSE); echo $my_date; ?> &nbsp;|&nbsp;&nbsp;<span class="wkl-people"></span>&nbsp;<?php the_author(); ?>
                            </p>
                            
                            <a href="<?php the_permalink(); ?>" class="btn border-btn2 reading-btn">
                            Continue Reading</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div> 
        </div>
    </section> -->
    
   
<?php
get_footer(); ?>
