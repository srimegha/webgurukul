<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Technical Workshop</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->
<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">What is Technical Workshop (YAGYA)?</h1>
		<div class="col-md-9 web-design-info">
			<p>
				In the ancient era, we organize a "Yagya" for any kind of good and pure results. Even we do the same thing in the present. Webgurukul-Yagya is the same thing in the technical Session where we give a specific training to students in the form of Workshop to which we have name “Yagya”. Yagya is the technical workshop conducted by webgurukul at different educational institutes in India. The main motive of Yagya is to provide quality training to students on recent trends in technology which helps them to stand in the corporate world. We generally focus on quality education and we deliver that too.
			</p>
			<p>
				We have successfully conducted "Yagya" at more than 15+ leading engineering institutes with more than 3000+ students participated till date in very short span of time. In "Yagya" we focus on C, C++, Data structure, which helps students to face the various company placement tests, visiting their college campus. We also deliver a session on HTML5-CSS3, PHP-MySQL, JS so that students will get the knowledge of web development industries and they will be ready to work for them too.
			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-web-development-icon course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="http://wpwebgurukul.webgurukul.co.in/wp-content/uploads/2017/12/Web-Development.pdf" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot grey-section tech-workshop">
	<div class="container">
		<h2 class="text-center">Why YAGYA?</h2>
		<p class="sec-subheading text-center">Yagya is a power packed session especially for engineering students with the latest bunch of courses which make them deliver them-self with highly updated knowledge of technology. Yagya is complete programs for students which build confidence in students to work with recent trends in technology.</p>
		<div class="course-row no-padding">
            <div class="col-md-4 col-sm-6 col-xs-12 margin-right-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<img src="<?php bloginfo("template_directory"); ?>/img/expert-panel.svg">
                    </div>
                    <!-- subheading -->
                    <h3>Expert Panel of Trainers</h3>
                    <!-- para -->
                    <p>With 5+ years of experience in corporate training and having knowledge in multiple domains.</p>
                    <!-- <a href="javascript:void(0)" class="btn border-btn1 courses-border-btn1">More Details</a> -->
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<img src="<?php bloginfo("template_directory"); ?>/img/book.svg">
                    </div>
                    <!-- subheading -->
                    <h3>More Knowledge in Less Time.</h3>
                    <!-- para -->
                    <p>We provide the best knowledge in less time in workshop sessions yo improve students technical skill</p>
                    <!-- <a href="javascript:void(0)" class="btn border-btn1 courses-border-btn1">More Details</a> -->
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<img src="<?php bloginfo("template_directory"); ?>/img/practical-approach.svg">
                    </div>
                    <!-- subheading -->
                    <h3>Full Practical Approach</h3>
                    <!-- para -->
                    <p>we provide practical knowledge to students and Industry oriented practice.</p>
                    <!-- <a href="javascript:void(0)" class="btn border-btn1 courses-border-btn1">More Details</a> -->
                </div>
            </div>
        </div>
        <div class="course-row no-padding clearfix">
            <div class="col-md-4 col-sm-6 col-xs-12 ">
               <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<img src="<?php bloginfo("template_directory"); ?>/img/practical-approach.svg">
                    </div>
                    <!-- subheading -->
                    <h3>Daily Query Solving Session</h3>
                    <!-- para -->
                    <p>We arrange daily query solving sessions for students. In this session, students can ask their queries.</p>
                    <!-- <a href="javascript:void(0)" class="btn border-btn1 courses-border-btn1">More Details</a> -->
                </div>
            </div>
        </div>      
	</div>
</section>
<!--end courses-module section -->

<section class="course-reviews padding-top-bot">
	<div class="container">
		<h2 class="text-center">YAGYA Courses</h2>
		<p class="sec-subheading text-center">In Yagya, there are not only web designing and development but the others too.</p>
		<div class="row course-reviews-row">
			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="row">
						<img src="<?php bloginfo("template_directory"); ?>/img/education-yagya1.svg">
					</div>
					<div class="row">
						<h3 class="course-details-headings text-center">UI Designing Courses</h3>
						<p>Covering all the web designing languages like HTML, CSS but also framework like bootstrap with full responsive designs of websites.</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="row">
						<img src="<?php bloginfo("template_directory"); ?>/img/education-yagya2.svg">
					</div>
					<div class="row">
						<h3 class="course-details-headings text-center">Development & Programming Courses</h3>
						<p>Here we are going hand in hand with programming and development languages. We are covering here PHP, MySQL also C and C++.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>