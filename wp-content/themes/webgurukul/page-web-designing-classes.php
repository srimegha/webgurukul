<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Web Designing Classes</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course page section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn Web Design Course in Nagpur </h1>
		<div class="col-md-9 web-design-info">
			<p>
				There are many Web Designing Courses training institutions in Nagpur, but WebGurukul is a best IT Training Institute in Nagpur that trains you from the basics of Web Designing Course theoretically and practically. Here we make you master in Web Designing by providing excellent coaching with live practical orientation. The advance Web Design Course is for those who are looking bright career in IT Sector, Webgurukul is the best place to make your bright career in IT Field.
			</p>
			<p>
				If you are passionate about web designing WebGurukul is the perfect place. We will provide perfect training and Guidance, this Web Design Course in Nagpur and Wardha. Total Duration of 3 months to 6 months you are our responsibility. After 3 to 6 Months, what??? You will be perfect in Web Designing and Job Ready. Join Today Best IT Training Institute Webgurukul and being a master in Web Designing.
			</p>
			<p>
				Webgurukul is a Leading IT Training Institute in Nagpur, We offer job oriented advanced Web Design Course in this course you will teach how to Design Responsive and Attractive website. Web Designing course content and syllabus based on industry requirement to achieve everyone's career goal. If you are thinking the career in IT Sector, Join job oriented Web Design Course at Webgurukul. Our Team will make you Best Coder in Web Design. Join Webgurukul.
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-web-designing-icon course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course page section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot grey-section web-designing">
	<div class="container">
		<h2 class="text-center">Get your Web Design Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class="course-row no-padding">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-htmlncss2 course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>HTML5 &amp; CSS3</h3>
                    <!-- para -->
                    <p>HTML5 and CSS3 are the languages you can use to build and style websites. In these courses, you’ll learn the basics of HTML and CSS.</p>
                    <a href="<?php echo get_site_url(); ?>/html-css-language" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                        <span class="wkl-RWD course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Responsive Web Design</h3>
                    <!-- para -->
                    <p>Responsive Web design is the approach that suggests that design and development should respond to the user’s behavior and environment based on screen size, platform and orientation.</p>
                    <a href="<?php echo get_site_url(); ?>/responsive-webdesign" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12 margin-right-15">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-Boostrap_logo course-icon"><span class="path1"></span><span class="path2"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>BootStrap</h3>
                    <!-- para -->
                    <p>Bootstrap is a framework which allows user to create a responsive website. In this course you'll learn to use Bootstrap and to customize it.</p>
                    <a href="<?php echo get_site_url(); ?>/bootstrap-technology" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-Adobe_Photoshop course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Photoshop</h3>
                    <!-- para -->
                    <p>Photoshop is very important for working with web designing. Learn how to work with images, shapes and all components in Photoshop.</p>
                    <a href="<?php echo get_site_url(); ?>/photoshop-design" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>
        <div class="course-row no-padding clearfix">
            <div class="col-md-4 col-sm-6 col-xs-12 ">
               <div class="course-module-div col-xs-12 margin-right-15">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-JSnJquery course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>JavaScript &amp; jQuery</h3>
                    <!-- para -->
                    <p>jQuery has become perhaps the most popular tool in use today for the design and implementation of JavaScript in web pages, and this course will introduce you to the basics of this dynamic cross-browser library. </p>
                    <a href="<?php echo get_site_url(); ?>/javascript-jquery-training" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
           
            
        </div>      
	</div>
</section>
<!--end courses-module section -->

<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Web Designing Course</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Front end Designer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="<?php echo get_site_url(); ?>/contact" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!--end course page register and contact section -->



<?php get_footer(); ?>