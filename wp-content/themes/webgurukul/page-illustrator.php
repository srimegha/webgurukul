<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Illustrator</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 --> 
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Illustrator Training In Nagpur</h1>
				<p><strong>Illustrator</strong> is the world's leading vector illustration software that can be used to accomplish many different design tasks. This course of <strong>Webgurukul</strong> trains you in core concepts and techniques that can be applied to any workflow: for print, for the web, or for assets that will find their way into other applications. Our trainers show you how to get around the interface, and explains elements of Illustrator, such as artboards, workspaces, layers, and shapes. Also discusses vector graphics which are composed of paths, strokes, and fills and shows how to create and edit them using the Illustrator drawing tools. </p>
                <p>In this course you going to learn how to combine and clean up paths and organize artwork into groups and layers. Also covers typography and text editing, color, expressive brush drawing, effects, printing and export, and much more. </p>
                <p>If you are looking for highly advanced training and <strong>professional Illustrator training</strong> consists of Designing of Icons, infographics, graphics, Visiting Cards, Letter Head, Envelopes, Brochures etc., then surely <strong>Webgurukul’s Illustrator training</strong> will help you master all the techniques. Start here for everything you need to know to design, draw, and illustrate with <strong>Illustrator.</strong></p>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Illustrator Modules</h3>
				<p>Illustrator is the world's leading vector illustration software that can be used to accomplish many different design tasks.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to Adobe Illustrator <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Adobe Illustrator?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Touring the Illustrator interface </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring the Illustrator's Tools panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating new documents</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Modifying and saving your documents </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Setting up your Illustrator preferences</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Exploring the Toolbox<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with all Selection tools </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic Painting tools</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with Artboards</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Drawing and Editing Shapes using shape tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating text with Text tool</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Working with Layers and Artboard<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring the Layers panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating and editing layers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with sublayers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Hiding, locking, and deleting layers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Targeting objects inside the Layers panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring the artboard panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating, Modifying artboard</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Drawing and transforming shapes<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Understanding vector path</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Drawing all type of shapes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Moving and duplicating objects </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using isolation mode to edit grouped artwork </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Scaling, Rotating and reflecting objects </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Creating Complex Shapes<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating compound paths and shapes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with the Pathfinder panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using the Illustrator Brush tools</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using the Eraser tool</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating artwork with the Shape Builder tool</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Using Raster Graphics in Illustrator<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Placing linked images into Illustrator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with the Links panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Embedding images into your Illustrator documents</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using clipping masks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using the Image Trace panel</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Converting pixels into paths</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Creating designing work<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating different logos</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Tracing Artworks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with print media: Visiting card, brochure etc.</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating icons, Illustration and infographics</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Color combinations and gradients</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Printing and Exporting Artwork<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Printing your artwork </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Saving files for print using PDF</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exporting web assets from Illustrator</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Saving in legacy formats</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Designing your own logos and create special effects.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use of all tools in Illustrator with their functionality. </p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Tracing and exporting all artwork in different format.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>You are able to design Brochure, Visiting card, infographics &amp; many more.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Basic template designing for portfolio website.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Making of graphics, avatars, web design and Icons.</p>
						</div>
					</div>
				</div>
			</div>

			
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-illustrator course-icon3"><span class="path1"></span><span class="path2"></span></span>
                </div>
                <P 	class="xlg">Illustrator</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-GraphicsDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-illustrator course-icon3"><span class="path1"></span><span class="path2"></span></span>
            </div>
            <P 	class="xlg">Illustrator</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-GraphicsDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<?php get_footer(); ?>