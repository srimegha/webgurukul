<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Web Development Classes</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn Web Development Course in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				We Trainers at WebGurukul are providing Best Web Development Course in Nagpur with hands-on practice for live projects, Here we will make you master in Web Development by providing excellent coaching with live practical orientation. The advance Web Development Course is for those who are looking bright career in IT Sector, we Webgurukul provides the best place to make your bright career in IT Field.
			</p>
			<p>
				If you are passionate about web development and making your career as a developer, Webgurukul is a perfect place. In our institute we offer the job oriented advanced Web Development Course. In this course you will learn how to Develop web applications, web services, logic building and many more related exercises. We are offering the course content which is preferred by industries that will help you to get your preferred Jobs.
			</p>
			<p>
				In this Web Development Course, our expert faculties will train you in programming languages like PHP, JavaScript, JQuery, Database Connectivity and related frameworks if you are planning for internship Programs. If you are thinking the career in IT Sector, Join job oriented Web Development Course at Webgurukul. Our Team will make you Best Coder in Web Development.

			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-web-development-icon course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!-- courses-module section -->
<section class="course-module padding-top-bot grey-section">
	<div class="container">
		<h2 class="text-center">Get your Web Development Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class=" course-row no-padding">
            <div class="col-md-4 col-sm-6 col-xs-12 margin-right-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-htmlncss2 course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>HTML5 & CSS3</h3>
                    <!-- para -->
                    <p>HTML5 and CSS3 are the languages you can use to build and style websites. In these courses, you’ll learn the basics of HTML and CSS.</p>
                    <a href="<?php echo get_site_url(); ?>/html-css-language" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-php course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>PHP</h3>
                    <!-- para -->
                    <p>PHP is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.</p>
                    <a href="<?php echo get_site_url(); ?>/php-training" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-sql course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span><span class="path51"></span><span class="path52"></span><span class="path53"></span><span class="path54"></span><span class="path55"></span><span class="path56"></span><span class="path57"></span><span class="path58"></span><span class="path59"></span><span class="path60"></span><span class="path61"></span><span class="path62"></span><span class="path63"></span><span class="path64"></span><span class="path65"></span><span class="path66"></span><span class="path67"></span><span class="path68"></span><span class="path69"></span><span class="path70"></span><span class="path71"></span><span class="path72"></span><span class="path73"></span><span class="path74"></span><span class="path75"></span><span class="path76"></span><span class="path77"></span><span class="path78"></span><span class="path79"></span><span class="path80"></span><span class="path81"></span><span class="path82"></span><span class="path83"></span><span class="path84"></span><span class="path85"></span><span class="path86"></span><span class="path87"></span><span class="path88"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>MySQL</h3>
                    <!-- para -->
                    <p>SQL, 'Structured Query Language', is a programming language designed to manage data stored in relational databases. SQL operates through simple, declarative statements.</p>
                    <a href="<?php echo get_site_url(); ?>/my-sql-training" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>
        <div class=" course-row no-padding">
            <div class="col-md-4 col-sm-6 col-xs-12 margin-right-15">
               <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-JSnJquery course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>JavaScript & jQuery</h3>
                    <!-- para -->
                    <p>jQuery has become perhaps the most popular tool in use today for the design and implementation of JavaScript in web pages, and this course will introduce you to the basics of this dynamic cross-browser library.</p>
                    <a href="<?php echo get_site_url(); ?>/javascript-jquery-training" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-ajax course-icon"></span>
                    </div>
                    <!-- subheading -->
                    <h3>Ajax</h3>
                    <!-- para -->
                    <p>AJAX is an important front-end web technology that lets JavaScript communicate with a web server. It lets you load new content without leaving the current page, creating a better, faster experience for your web site's visitors.</p>
                    <a href="<?php echo get_site_url(); ?>/ajax-language-training" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div> 
            </div>
            
        </div>      
	</div>
</section>
<!-- end courses-module section -->


<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Web Development Classes</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Front end Designer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="<?php echo get_site_url(); ?>/contact" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!-- end course page register and contact section -->



<?php get_footer(); ?>