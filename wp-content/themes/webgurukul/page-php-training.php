<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">PHP</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>PHP Training In Nagpur</h1>
				<p>If you are planning your career as a web developer, you can start learning PHP with the best PHP Course provided by WebGurukul Nagpur, Wardha. We recommend you before starting your career in PHP development you must aware of basics of HTML and CSS, you can join our IT Internship Program to get more Job opportunities.  In this course, you will get to know to make how to make projects with PHP, Knowledge of MySQL database and how to deal with backend programming. It is very useful and Job Oriented IT Course.</p>
				<p>Webgurukul is a Leading PHP Training Institute in Nagpur. We also provide our PHP training in Wardha. We offer job Oriented Advanced IT Courses like PHP, Laravel, CakePHP. After joining Webgurukul you will get major chances to get placed with IT Companies. Your sincere efforts and regular practice always support you to get trained and to achieve your first job in IT.</p>
				
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of HTML and CSS.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">PHP Modules</h3>
				<p>PHP is a popular programming language that you can use to write simple code for web pages. If you have been using HTML to develop websites, learning PHP will allow you to create dynamic pages. In this course,  you will learn the fundamentals of PHP. The course covers concepts such as how to embed PHP code into an HTML page, and reviews the basic PHP data types such as strings and arrays. The course also covers the different control structures in PHP, how to work with built-in PHP functions, and how to define your own custom functions.</p>
				<p>PHP is  one of the the most popular server-side language used to build dynamic websites.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1:	Introduction to PHP<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Installation</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Syntax Overview</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Variables in PHP<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Variables Types</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Constants<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Constants in PHP</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4:	Operators<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Operators in PHP
								</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: 	Decision Making<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>If Statement</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>If Else</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>ElseIF</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Switch</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: 	PHP LOOP<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>For Loop</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>While Loop</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Do-While Loop</span></a></li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>For Each</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: 	PHP Arrays<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Numeric Array</span></a></li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Associative Array</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Multidimensional Array</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8:	PHP Strings</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9:	PHP-WEB Concept</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: PHP GET & POST</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: PHP File Inclusion</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: PHP Files and I/O</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 13: PHP Functions</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 14: PHP Cookies & Session</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 15: PHP Email</a>
					</li>
					
					<li>
						<a href="javascript:void(0)" class="lg">Module 16: PHP File Upload</a>
					</li>
					
					<li>
						<a href="javascript:void(0)" class="lg">Module 17: 	PHP & MySQL<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Connection</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>MySQL Operations with PHP</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 18: PHP Form Example<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Intrduction</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Validation Example</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Complete Scope</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 19: PHP Login Example<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Login Example</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>PHP-MySQL Login Example</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 20: OOPS in PHP<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>PHP Classes</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Objects in PHP</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Member Function</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Constructor & Destructor</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Functions Overriding</span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><span class="wkl-paper"></span><span>Access Specifies</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 21: PHP AJAX Examples</a>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block 
			col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Build simple PHP website s quicky</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Update PHP webpages easily</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Setup local Apache server to render PHP files</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand PHP data types</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Write basic PHP Syntax</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use echo and print statements to display text</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use various types of PHP Operators</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create and use variables</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand variable scopes</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create and understand arrays indexing</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create functions</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use conditional statements</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use Loops</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use Include statements to assemble web pages</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-php course-icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span>
                </div>
                <p 	class="xlg">PHP</p>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-php course-icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span>
            </div>
            <p 	class="xlg">PHP</p>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDevelopmentCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
		<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>