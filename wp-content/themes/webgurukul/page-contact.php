<?php get_header(); ?>


<!-- banner -->
<section class="contact-banner link-page-banner bg-image">
	<h2 class="banner-heading">Contact Us</h2>
	<ul class="breadcrumb">
		<?php if ( function_exists('yoast_breadcrumb') ) 
                {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } 
            ?>
	</ul>
</section>
<!-- end banner -->

<!-- contact-us content-->
<section class="contact-page link-page padding-top-bot">
	<div class="container">
		<h1>Contact Details</h1>
		<div class="row">
			<div class="col-md-7 col-xs-12 contact-form">
                <p class="sec-subheading col-xs-12">Looking to contact us? Pick the option that fits your inquiry best!</p>
                <?php echo do_shortcode( '[contact-form-7 id="70" title="Contact form"]') ?>
				
			</div>
			<div class="col-md-5 col-xs-12 contact-info">
                <div class="row cont-numb">
                    <div class="col-xs-1 no-padding">
                        <span class="wkl-technology contact-page-icon">
                        </span>
                    </div>         
                    <div class="col-xs-11">
                        <p class="sm cont-color1">Call us</p>
                        <p class="lg">+91-8237733112 | +91-7387990061</p>
                    </div>     
                </div>  
                <div class="row cont-mail">
                    <div class="col-xs-1 no-padding">
                        <span class="wkl-message contact-page-icon"></span>
                    </div>         
                    <div class="col-xs-11">
                        <p class="sm cont-color1">Write us</p>
                        <p class="lg">edu@webgurukul.co.in </p>
                    </div>     
                </div>  
                <div class="row cont-add">
                    <div class="col-xs-1 no-padding">
                        <span class="wkl-location contact-page-icon"></span>
                    </div>         
                    <div class="col-xs-11">
                        <p class="sm cont-color1">Visit us</p>
                        <p class="lg addr1">IT Park Nagpur:</p>
                        <p class="sm cont-color2">Plot No. 11, Madhav Nagar, Behind Domino's 
                        Pizza, Near Mate Square, Nagpur - 440010.</p>
                        <a class="sm get-direction" href="https://www.google.co.in/maps/place/Webakruti/@21.122658,79.0539073,17z/data=!3m1!4b1!4m5!3m4!1s0x3bd4c07900bc2f59:0xd911254f7ff8a237!8m2!3d21.122658!4d79.056096" target="_blank">Get direction<span class="wkl-contact-page-arrow contact-page-arrow"></span></a>

                        <p class="lg addr2">Near Medical Square, Nagpur:</p>
                        <p class="sm cont-color2">1st Floor, Plot No.351, Great Nag Road, Untkhana, Nagpur, Maharashtra - 440009. </p>
                        <a class="sm get-direction" href="https://www.google.co.in/maps/place/Webakruti/@21.122658,79.0539073,17z/data=!3m1!4b1!4m5!3m4!1s0x3bd4c07900bc2f59:0xd911254f7ff8a237!8m2!3d21.122658!4d79.056096" target="_blank">Get direction<span class="wkl-contact-page-arrow contact-page-arrow"></span></a>

                        <p class="lg addr2">Wardha:</p>
                        <p class="sm cont-color2">2nd floor, Above Patni Multi-Speciality Dental Clinic, Near Wanjari Chowk, Wardha -442001.</p>
                         <a class="sm get-direction" href="https://www.google.co.in/maps/place/Webakruti+Wardha/@20.7412342,78.5974425,18z/data=!4m5!3m4!1s0x3bd47f123b8cb56b:0x873b8900b7ec9195!8m2!3d20.741455!4d78.5983469" target="_blank">Get direction<span class="wkl-contact-page-arrow contact-page-arrow"></span></a> 
                    </div>     
                </div> <!-- end cont-add -->
            </div> <!-- contact-info -->
		</div> 
	</div>
</section>

<?php get_footer(); ?>
<script type="text/javascript">
    //     $('input').click(function(){
    //        inpid =  $(this).attr('id');
    //     $( "label" ).removeClass('active');
    //     $( "label[for="+inpid+"]" ).addClass('active');
    // });
    $(document).ready(function(){
        
            $('.mdc-layout-grid').removeClass('mdc-layout-grid');
            $('.cf7md-admin-customize-message').css("display","none");

        
    });
</script>