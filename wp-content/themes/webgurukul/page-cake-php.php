<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Cake PHP</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Cake PHP Training In Nagpur</h1>
				<p>If you are thinking to become a successful Web Designer and Developer. So first you must join HTML and CSS Training. This two course is the base of Web Technology. In this course, you will get to know to make web pages and how to give graphics to your website. It is very useful and Job Oriented IT Course.</p>
				<p>Webgurukul is a Leading IT Training Institute in Nagpur. We also provide our IT training in Wardha. We offer job Oriented Advanced IT Courses. After teaching in Webgurukul you will get 100% JOB in IT Sector. Because once you learn in webgurukul you will confident in your selected course and you will get Fast job in India or in Nagpur IT Park.</p>
				<p>Webgurukul IT Training Institute trained 3000+ Students in Nagpur and wardha. All our students placed in Reputed IT Companies and In Organization all over India. We have an expert faculty for HTML 5 and CSS3 Training.In Nagpur, there are So many coaching they doing business only for money. But webgurukul Don't, We will train you like Expert, After joining this Course what you will See?, You will see you become a professional in HTML 5 and CSS3 Training. JOIN Webgurukul TODAY.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of HTML, CSS and PHP.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Cake PHP Modules</h3>
				<p>CakePHP is designed to make common web-development tasks simple, and easy. By providing an all-in-one toolbox to get you started the various parts of CakePHP work well together or separately.</p>
				<p>CakePHP provides a basic organizational structure that covers class names, filenames, database table names, and other conventions. While the conventions take some time to learn. </p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Getting Started<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Installation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Configuration</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Routing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Request & Response Object</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Middleware</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Controllers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>View</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Database Access & ORM</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Using Cake PHP<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using CakePHP</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Authentication</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Bake Console</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Caching</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Console Tools, Shells & Tasks</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Debugging</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Email</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Error & Exception Handling</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Events System</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Events System</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Internationalization & Localization</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Logging</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Modelless Forms</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Pagination</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Plugins</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>REST</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Security</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Sessions</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Testing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Validation</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Understand CakePHP</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Setup an entire Application</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Encrease security in your CakePHP application</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use CakePHP TimeHelper</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use CSRF Component</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Add a captcha image to your application</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Use Jquery DataTables</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Get a good basics in crud concepts in CakePHP</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-cakephp course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                </div>
                <P 	class="xlg">Cake PHP</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-FrameworkCourses.pdf" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-cakephp course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
            </div>
            <P 	class="xlg">Cake PHP</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-FrameworkCourses.pdf" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>