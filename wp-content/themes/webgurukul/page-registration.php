<?php get_header(); ?>

<section class="registration-page padding-top-bot grey-section">
	<div class="container-fluid">
		<div class="col-md-4 col-xs-12 ">
			<div class="reg-form">
				<div class="sec-heading">
					<?php               
			            $args = array(
			                'post_type'=> 'pop_notification'     
			            );
			            query_posts( $args );
			              while ( have_posts() ) : the_post();
			        ?>
			        <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive" alt="<?php the_title(); ?>">
			        <?php endwhile; ?>				
				</div>
				<div class="temp-links">
					<p class="lg1">Know More</p>
					<div class="clearfix">
						<a class="lg pull-left btn fill-btn1" href="<?php get_site_url(); ?>/web-designing-classes" target="_blank">Web Designing</a>
						<a class="lg pull-right btn fill-btn1" href="<?php get_site_url(); ?>/web-development-classes" target="_blank">Web Development</a>	
					</div>
				</div>
			</div>
		</div>
        <div class="reg-form col-md-8 col-xs-12">
            <h2 class="clr-orange col-xs-12">Registration Form</h2>
            <?php echo do_shortcode( '[contact-form-7 id="212" title="Registration Form"]') ?>
    		
         </div>
	</div>
</section>

 <?php get_footer(); ?>