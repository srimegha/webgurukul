<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<p class="banner-heading">Software Course</p>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!--course section-1  -->
<section class="learn-web-design link-page padding-top-bot">
	<div class="container">
		<h1 class="sec-heading col-xs-12">Learn Software Course in Nagpur</h1>
		<div class="col-md-9 web-design-info">
			<p>
				Huge demand and popularity of Software Development Courses, In Nagpur there are a lot of software development courses available for freshers, students, and professionals. Software development course is nothing but commands, syntax and programming. They cover full software cycle. Our faculty will teach you to create algorithms and special practice on programming coding in software training.Through full software development course you will learn programming, Profiling, debugging, software development cycle and many more topics will cover.
			</p>
			<p>
				Now webgurukul offers you JOB oriented software development courses, now its depend on you which course you will choose. IT professionals and Software Engineers a rise of more than 42 Percent in upcoming years. It means that 4 Lac jobs will be added in job market up to the year 2018 -2019. Software engineers demand to raise in all sectors like Healthcare IT, Software Industries, ERP companies, Web Development Companies, E-commerce Sector and much more.
			</p>
		</div>
		<div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-2 no-padding course-brochure">
			<div class="course-brochure-div bg-image">
				<span class="wkl-web-development-icon course-icon1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
				<p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn register1-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn courses-border-btn2 download1-btn">Download Brochure </a>
				</p>
			</div>	
		</div>
	</div>
</section>
<!--end course section-1  -->

<!--courses-module section -->
<section class="course-module padding-top-bot grey-section">
	<div class="container">
		<h2 class="text-center">Get your Software Course Modules</h2>
		<p class="sec-subheading text-center">See which courses will help you reach your goal.</p>
		<div class="row course-row no-padding">
            <div class="col-md-4 col-sm-12 margin-right-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-reactjs course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>React JS</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="react-js.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-angularjs course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Angular JS</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="angular-js.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-nodejs course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Node JS</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="node-js.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>
        <div class="row course-row no-padding">
            <div class="col-md-4 col-sm-12 margin-right-15">
               <div class="course-module-div 
               col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-wordpress course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Advance WordPress</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="advance-wordpress.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                    	<span class="wkl-cakephp course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Cake PHP</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="cake-php.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div> 
            </div>
            <div class="col-md-4 col-sm-12 margin-left-15">
                <div class="course-module-div col-xs-12">
                    <!-- icon-circle -->
                    <div class="course-circle">
                   		<span class="wkl-advance-UI course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
                    </div>
                    <!-- subheading -->
                    <h3>Advance UI design</h3>
                    <!-- para -->
                    <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                    <a href="advance-ui.php" class="btn border-btn1 courses-border-btn1">More Details</a>
                </div>
            </div>
        </div>      
	</div>
</section>
<!--end courses-module section -->

<!-- what to learn section -->
<section class="what-to-learn padding-top-bot">
	<div class="container">
		<h2>What will you are going to Learn?</h2>
		<p class="sec-subheading">Our Expert Faculty will teach you all software course modules.</p>
		<!-- row-1 -->
		<div class="row what-to-learn-row">
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Web Designing Concept</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Website Layout</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Attractive website Theme </p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
		</div>
		<!-- row-2 -->
		<div class="row what-to-learn-row">
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Antinational Website Design</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Photo Base Website Making</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Single Page website Designs</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
		</div>
		<!-- row-3 -->
		<div class="row what-to-learn-row">
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Attractive Forms Designs</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Responsive Web Design</p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 what-to-learn-div">
				<div class="col-xs-1 check-icon1 no-padding">
					<span class="wkl-check-mark-button"></span>
				</div>
				<div class="col-xs-11">
					<p class=lg>Creative Design Making </p>
					<p>Create static HTML and CSS portfolio 
					sites and landing pages.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!--end what to learn section -->

<!-- course page register and contact section -->
<section class="course-page-contact padding-top-bot bg-image">
	<div class="container">
		<h2>Do you want to Join Software
		 Course</h2>
		<p class="sec-subheading">Make your first step towords IT Industry. Become a Front end Designer in short time. Learn from company experts.</p>
		<a href="<?php echo get_site_url(); ?>/registration" class="btn fill-btn2 courses-fill-btn">Register Now</a>
		<a href="contact.php" class="btn courses-border-btn2 contact-btn1">Contact Us</a>
	</div>
</section>
<!--end course page register and contact section -->

<!-- course page review section -->
<section class="course-reviews padding-top-bot">
	<div class="container">
		<h2 class="review-heading">Course Rewiews (174)</h2>
		<div class="row course-reviews-row">
			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="<?php bloginfo('template_directory'); ?>/img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="<?php bloginfo('template_directory'); ?>/img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row course-reviews-row">
			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="<?php bloginfo('template_directory'); ?>/img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="course-review-div">
					<div class="col-xs-2">
						<img class="img-responsive imgwidth-100" src="<?php bloginfo('template_directory'); ?>/img/smily-image.png">
					</div>
					<div class="col-xs-10 no-padding review1">
						<p class="review-subheading">GinevraSapphireBlue<br>
						<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
						</p>
						<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
					</div>
				</div>
			</div>
		</div>
		<p class="text-center"> 
			<a href="JavaScript:Void(0);" class="btn border-btn1 courses-border-btn1">See More</a>
		</p>
	</div>
</section>
<!--end course page review section -->

<?php get_footer(); ?>