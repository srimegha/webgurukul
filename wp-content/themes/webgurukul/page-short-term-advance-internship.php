<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">45 Days Internship Program</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Learn 45 Days Internship Program in Nagpur</h1>
				<p>There are many Web Designing Courses in Nagpur. But Webgurukul is only the best destination webgurukul is a best IT Training institute in Nagpur that teaches you from very basic in Web Designing Course practically. Here we make you master in Web Designing by providing excellent coaching with live practical orientation. The advance Web Design Course is for those who are looking bright career in IT Sector, Webgurukul is the best place to make your bright career in IT Field.</p>
				<p>If you are passionate about web designing Webgurukul is a perfect place. We will provide perfect training and Guidance, this Web Design Course in Nagpur and Wardha. Join Today Best IT Training Institute Webgurukul and being a master in Web Designing.
				</p>
				<p>
				Webgurukul is a Leading IT Training Institute in Nagpur, We offers Job oriented advanced Web Design Course in this course you will teach how to design Responsive and Attractive website. Web Designing course content and syllabus based on students requirement to achieve everyone's career goal. If you are thinking career in IT Sector, Join job oriented Web Design Course at Webgurukul. Our Team will make you Best Coder in Web Design. </p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>Graduate and Undergraduate</span></li>
					<li><span>Fresher’s</span></li>
					<li><span>Professionals</span></li>
					<li><span>Job Seekers</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<!-- <li><span>Basic Knowledge of HTML ad CSS</span></li>
					<li><span>HTML 5</span></li>
					<li><span>CSS 3</span></li> -->
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of Photoshop else alright if don’t know.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Training Modules</h3>
				<p>CSS is a languages that can use to build Style in website. In these courses, you’ll learn the all basics of CSS, build your first beutiful website, and then review some of the current CSS3 best practices. Be ready with all concepts of CSS and CSS3.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: HTML5<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Web</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>HTML Basic</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Difference between dynamic and static website</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Folder structure for Website </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is DOCType? </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Tags</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Attributes </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Elements </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Table </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Div </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Box Model </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: CSS3 <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to CSS </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Types of CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS syntax, CSS Id &amp; Class </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Float properties</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Selectors </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Border </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Fonts  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Background </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Gradient </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Position </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Layout design </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Template Design </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: One Template Designing </a>						
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Introduction to PHP <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Software installation  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Web  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Internet and web site development  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to Client and Server architecture</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic Syntax  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Comments  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Types and  Variable   </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Constants and Literals </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Storage Classes </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Operators  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Loop Type </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Decision Making  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Functions, Arrays and Strings   </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Database connectivity </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>GET, POST method </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Cookies and sessions </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Image uploading</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Modules (Registration, Login, View, Edit Delete) with Database</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Introduction to MySQL <i class="wkl-add list-toggle-icon"></i></a>					
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction about RDBMS </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Basics of SQL </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data types </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Definition </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create and Drop </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Use and Show </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Alter and Describe </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Data Manipulation </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Insert </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Select and Where </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Order By  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Update  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Delete </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Introduction to CRUD &amp; Sessions</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: PHP Project Flow Implementation</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Introduction to JavaScript, jQuery &amp; Ajax <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction to JavaScript   </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>String Method </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>String to Array </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Math Object </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JavaScript Validation </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JavaScript Output</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Extracting string characters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Variable to Numbers</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery Effect</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery Selector</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery DOM Manipulation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery JSON</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery Validation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery Event Handling</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery Slider</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>jQuery AJAX</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: Introduction to Bootstrap <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Responsive Web design </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Grid System</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Component </span></a></li>							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: Two Temapte Design </a>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: One Mini Project with Validations </a>
					</li>					
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Software Development Life Cycle process from Analysis to Deployment.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Hand on experience of Live Project.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Leadership, Team work, communication and corporate experience</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Work with technology expert to learn new things</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Individual work analysis and guidance</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Company Certificate of Corporate Internship</p>
						</div>
					</div>
				</div>
			</div>

			
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-Boostrap_logo course-icon3"><span class="path1"></span><span class="path2"></span></span>
                </div>
                <P 	class="xlg">45 Days Internship</P>
                <p class="text-center"> 
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-InternshipProgram.pdf" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-Boostrap_logo course-icon3"><span class="path1"></span><span class="path2"></span></span>
            </div>
            <P 	class="xlg">45 Days Internship</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-InternshipProgram.pdf" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->

<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>