<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Advance Photoshop</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block 
			col-xs-12">
				<h1>Advance Photoshop Training In Nagpur</h1>
				<p>This is the Webgurukul's highest level of Photoshop training, and is for those who want to become Photoshop power users. In this training, you will learn how to create complex composites with text and layer effects. Seamless integration of effects and colors for realistic images is covered, along with many advanced tips that are helpful for even the most experienced users.</p>
				<p>Learn the techniques necessary to create, manipulate and edit professional quality images with skill and precision from our expert trainers who have all extensive industry experience as designers.</p>
				<p>If you’ve been using Photoshop for a while now but would like to develop your imaging skills, Webgurukul's Photoshop Advance course will give you the boost you need. Emphasis is placed on understanding the process and how to use a combination of functions to develop the effects you want. We explore various approaches to creating special effects, and delve into new features as well as features of the application you may not have known existed. Along with developing your image editing techniques you’ll develop your workflow skills through the use of shortcuts and automation to get your work looking better, faster.</p>
			</div>
			<div class="course-details-block2 course-details-block 
			col-xs-12">
				<!-- <p class="course-details-headings">Who can Apply?</p>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul> -->

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of Photoshop else alright if don’t know.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">Advance Photoshop Modules</h3>
				<p>Advance Photoshop is updated course to create complex composites with text and layer effects, along with many advanced tips and tricks.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Advanced Navigational Features<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with hidden tools</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Exploring advance menu options</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with Artboard  </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Use of channels </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Adding special Filter effects<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The power of Smart Filters</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using Camera Raw as a Smart Filter</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding Layer Style effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding interesting lighting effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding sunlight effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating a Depth of Field effect</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding text effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create Background pattern</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: Creating Animations and video editing<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Timeline Interface</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Image,Text,Layer Animation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Speed Animation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create Gif Animation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Video Animation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Cut, Split, Add Video</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Video Layer Effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Video Render </span></a></li>							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: Creation of mock-up using smart object<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Understanding smart object</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Creating shape for any devices</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Applying smart object in mock-up</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Customizing mock-up </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Branding designs creation<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Designing different types of logos</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Visiting card designing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with brochure, letter head, banner, flyer etc.</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: Advanced image re-touching and painting<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Content Painting </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Retouching an image</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using an advanced adjustment layer</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Removing portion from images</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Adding effects in image</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Organizing Layers panel with color</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: Designing bootstrap grid web template<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Getting started with bootstrap grid</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working with column and gutter space</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Designing sections with effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Responsive design for mobile and tablet.</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: Creating Mobile app Designs<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Understanding size, typography, layout of mobile</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Working different sections</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Saving in different formats</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block 
			col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Designing of logos and creation special effects for it.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Advanced image editing, special filter options . </p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Image animation, video editing and animating.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Able to design Brochure, Visiting card, letterhead &amp; many more.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Bootstrap web template desiging for any website with responsive.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Creation of Mobile app screen and work flow.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-Adobe_Photoshop course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div>
                <P 	class="xlg">Advance Photoshop</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-GraphicsDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-Adobe_Photoshop course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
            </div>
            <P 	class="xlg">Advance Photoshop</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-GraphicsDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>