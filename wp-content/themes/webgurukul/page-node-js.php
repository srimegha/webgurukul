<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">Node JS</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 -->
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>Node JS Training In Nagpur</h1>
				<p>If you are thinking to become a successful Web Designer and Developer. So first you must join HTML and CSS Training. This two course is the base of Web Technology. In this course, you will get to know to make web pages and how to give graphics to your website. It is very useful and Job Oriented IT Course.</p>
				<p>Webgurukul is a Leading IT Training Institute in Nagpur. We also provide our IT training in Wardha. We offer job Oriented Advanced IT Courses. After teaching in Webgurukul you will get 100% JOB in IT Sector. Because once you learn in webgurukul you will confident in your selected course and you will get Fast job in India or in Nagpur IT Park.</p>
				<p>Webgurukul IT Training Institute trained 3000+ Students in Nagpur and wardha. All our students placed in Reputed IT Companies and In Organization all over India. We have an expert faculty for HTML 5 and CSS3 Training.In Nagpur, there are So many coaching they doing business only for money. But webgurukul Don't, We will train you like Expert, After joining this Course what you will See?, You will see you become a professional in HTML 5 and CSS3 Training. JOIN Webgurukul TODAY.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<p class="course-details-headings">Who can Apply?</p>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<p class="course-details-headings">Required Knowledge</p>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of Photoshop else alright if don’t know.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<p class="course-details-headings">Node JS Modules</p>
				<p>CSS is a languages that can use to build Style in website. In these courses, you’ll learn the all basics of CSS, build your first beutiful website, and then review some of the current CSS3 best practices. Be ready with all concepts of CSS and CSS3.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to CSS 1.0 and 2.0<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Introduction to CSS3<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: CSS3 Borders<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: CSS3 Backgrounds<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: CSS3 Text Effects<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: CSS3 Fonts<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: CSS3 2D Transforms<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: CSS3 3D Transforms<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: CSS3 Transitions<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: CSS3 Animations<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: CSS3 Multiple Columns<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: CSS3 User Interface<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction of CSS and all basics for CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id & Class</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<p class="course-details-headings">What will you are going to Learn?</p>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="course-details-review course-details-block col-xs-12">
				<p class="course-details-headings">Course Reviews</p>
				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<div class="col-xs-12 course-reviews-row padding-left0">
					<div class="course-review-div col-xs-12 padding-left0">
						<div class="col-xs-1 padding-left0">
							<img class="img-responsive imgwidth-100" src="img/smily-image.png">
						</div>
						<div class="col-xs-11 no-padding review1">
							<p class="review-subheading">GinevraSapphireBlue<br>
							<span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span><span class="wkl-star"></span>
							</p>
							<p>I picked up pieces of html and css when I needed to create a very basic website to pass an introductory computer science course but there were lots of holes in my knowledge. I have just finished this course and I feel I am ready to tackle more advanced topics and not get lost while doing so.</p>
						</div>
					</div>
				</div>

				<p class="text-center"> 
					<a href="JavaScript:Void(0);" class="btn border-btn1 courses-border-btn1">See More</a>
				</p>
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-nodejs course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                </div>
                <P 	class="xlg">Node JS</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-nodejs course-icon3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
            </div>
            <P 	class="xlg">Node JS</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="JavaScript:Void(0);" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
               		 <span class="wkl-web-designing-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span></span>
                </div>
			               
                <h3>Website Designing<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="web-development.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-Graphics-design-course course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                </div>
           
                <h3>Graphics Design<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="digital-marketing.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
	            <div class="course-circle">
	           		<span class="wkl-web-development-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
	            </div>
	       
	            <h3>Website Development<br>Course</h3>
	      
	            <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
	            <a href="software-course.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
	            <div class="course-circle">
	           		<span class="wkl-advance-c-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
	            </div>
	       
	            <h3>Programming<br>Languages</h3>
	      
	            <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
	            <a href="programming-language.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-digital-marketing-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
                </div>
           
                <h3>Digital Marketing<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="web-design.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>

		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-employee course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>
                </div>
           
                <h3>Internship<br>Program</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="internship-program.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>

		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-coporate-training-icon course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div>
           
                <h3>Corporate<br>Training</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="corporate-tranning.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>

		<div class="item">
			<div class="course-module-div">
                <div class="course-circle">
               		<span class="wkl-PHP-Course course-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
                </div>
           
                <h3>Complete PHP<br>Course</h3>
          
                <p>Nowadays website is very important to all business because most of the users before going to market they check on online.</p>
                <a href="complete-php-course.php" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
	     </div>
                       
        </div>
        <div class="MS-controls">
            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

<!-- below768 -->
<section class="other-course-section1 padding-top-bot">
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<li><a href="web-development.php">Website Development Course</a></li>
				<li><a href="internship-program.php">Internship Program</a></li>
				<li><a href="programming-language.php">Programming Languages</li>
				<li><a href="web-design.php">Website Designing Course</li>
				<li><a href="corporate-tranning.php">Corporate Training</li>
				<li><a href="graphics-design.php">Graphics Design Course</li>
				<li><a href="digital-marketing.php">Digital Marketing Course</li>
				<li><a href="complete-php-course.php">Complete PHP Course</li>
			</ul>
		</div>
	</div>
</section>
<!-- end other course slider -->
<?php get_footer(); ?>