<?php get_header(); ?>

<!-- banner -->
<section class="course-1 course-page-banner bg-image">
	<div class="container">
		<h2 class="banner-heading">HTML5 AND CSS3</h2>
		<ul class="breadcrumb hidden-xs">
			<?php if ( function_exists('yoast_breadcrumb') ) 
				{
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} 
			?>
		</ul>
	</div>
</section>
<!-- end banner -->

<!-- course details section-1 --> 
<section class="course-details grey-section" id="content">
	<div class="container">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="course-details-block1 course-details-block col-xs-12">
				<h1>HTML5 and CSS3 Training In Nagpur</h1>
				<p>Website designing is an important part of IT industry. To be a good web designer you should have a complete knowledge of HTML and CSS. These are the basic requirements for designing a website. In today’s world of competition to be amongst at the top one needs media. Websites are playing a key role in today’s life.</p>
				<p>Many jobs are available for web designers than before. Many big companies demand experienced and quality web designers. The scope of web designing is always high. In today’s web and internet world, people want creative website when they are accessing it. Top company web development of India gives up to 8-10 lac per year for an experienced web designer. So its very good field for working and also it creative field.</p>
				<p>HTML5 and CSS3 represent numerous opportunities in web development for businesses that develop and deploy online content and web applications. Overall, by using both of these tools in your businesses’ web development you can optimize your users’ web experience, provide a solid foundation for your SEO and content marketing strategy and significantly reduce your cross-platform web development and support costs whilst increasing your reach and optimizing web experience.</p>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Advantages of HTML5 and CSS3</h3>
				<ul>
					<li><span>Cost effective Multi-Platform Development</span></li>
					<li><span>Good page ranking</span></li>
					<li><span>Offline browsing</span></li>
					<li><span>Consistency across multiple browsers</span></li>
					<li><span>Better mobile access to Business Intelligence</span></li>
					<li><span>Extension of video to a wide range of platforms</span></li>
					<li><span>Geolocation</span></li>
					<li><span>A better user experience</span></li>
				</ul>
			</div>
			<div class="course-details-block2 course-details-block col-xs-12">
				<h3 class="course-details-headings no-padding">Who can Apply?</h3>
				<ul>
					<li><span>All branch graduates or undergraduate students who want to learn web designing.</span></li>
					<li><span>All stream students.</span></li>
					<li><span>Job seekers to get quick job in IT field.</span></li>
					<li><span>Any who wants to learn a new skill or improve skill for there career.</span></li>
				</ul>

				<h3 class="course-details-headings no-padding">Required Knowledge</h3>
				<ul>
					<li><span>Basic computer knowledge.</span></li>
					<li><span>General knowledge of website working.</span></li>
					<li><span>Basics of Photoshop else alright if don’t know.</span></li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">HTML5 Modules</h3>
				<p>HTML stands for Hyper Text Markup Language. HTML5 is a revised version of the original HTML standard created in 1990 by the World Wide Web Consortium to define an Open Web Platform.  HTML is a language used for structuring and presenting content on the Web consistently, across web browsers. HTML5 is the evolution of that standard to meet the increasing demands presented by today's rich media, cross-device and mobile internet access requirements.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to Web <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>W3C and W3C Members</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why WHATWG?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Web?</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: HTML5 Basics <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Parts in HTML Document</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Editors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Basic</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Elements</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Attributes</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Headings</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Paragraphs</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Formatting</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Links</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Head</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Images</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Tables</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>JavaScript</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>HTML XHTML</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>HTML4 Drawbacks </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: HTML5 Introduction <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>HTML5 HISTORY</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New Features and groups</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Backward Compatibility</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Why HTML5?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Power of HTML5</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>m or mobi or touch domains</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Common Terms in HTML5 </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: HTML5 Syntax <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The DOCTYPE</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Character Encoding</span></a></li>							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: Obsolete Elements / Deprecated Elements <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < acronym > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < applet > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < basefont > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < big > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < center > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < dir > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < font > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < frame > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < frameset > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < isindex > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < noframes > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < s > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < strike > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < tt > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < u > </span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < xmp > </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: HTML5 New Elements <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New Semantic/Structural Elements</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < article ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < aside ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < bdi ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < command ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < details ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < dialog ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < summary ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < figure ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < figcaption ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < footer ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < header ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < mark ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < meter ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < nav ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < progress ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < ruby ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < rt ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < rp ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < section ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < time ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> < wbr ></span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: HTML5 Canvas <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Canvas?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create a Canvas</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Canvas Coordinates</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Canvas – Paths</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Canvas – Text</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Canvas – Gradients</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Canvas – Images </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: HTML5 SVG <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is SVG?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>SVG Advantages</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Differences Between SVG and Canvas</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Comparison of Canvas and SVG </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: HTML5 Drag / Drop <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Make an Element Draggable</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What to Drag? Where to Drop? </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: HTML5 Geolocation <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Locate the User’s Position+</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Handling Errors and Rejections</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>The getCurrentPosition()</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Geolocation object </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: HTML5 Video / Audio <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Video /  Audio on the Web</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How It Works?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Video / Audio Formats and Browser Support</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>HTML5 Video / Audio  Tags</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: HTML5 Input Types <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>color, date, datetime, datetime-local, email, month, number, range, search, tel, time, url, week</span></a></li>
							<!-- <li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Web?</span></a></li> -->
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 13: HTML5 Form Elements <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< datalist ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< keygen ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< Output ></span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 14: HTML5 Form Attributes <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New attributes for < form > &amp; < input > New attributes for < form ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>autocomplete</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>no validate </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 15: HTML5 New Attributes For Input Tag <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>autocomplete</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>autofocus</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>form, formaction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>formenctype, formmethod</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>formnovalidate</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>formtarget</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>height and width</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>list, min and max</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>multiple</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>pattern (regexp)</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>placeholder</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>required, step </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 16: HTML5 Semantic <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What are Semantic Elements?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Non-semantic elements: < div > &amp; < span ><br>
      							  Semantic elements: < form >, < table >, &amp; < img >
      							</span></a></li>					
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New Semantic Elements in HTML5</span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< header ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< nav ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< section ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< article ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< aside ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< figcaption ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< figure ></span></a></li>
      						<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< footer ></span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 17: HTML5 Media and Multimedia <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>New Media Elements</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< audio ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< video ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< embed ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< source ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>< track ></span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Multimedia</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Browser Support</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Multimedia Formats</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Video Formats</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Sound Formats</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 18: HTML5 - MathML <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Using MathML character</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Matrix Presentation</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Math Formulas</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 19: HTML5 Web Workers <i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>What is Web Worker ?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Check Web Worker support</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Create a WebWorker file</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Terminate a Web Worker</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-module-list course-details-block col-xs-12">
				<h3 class="course-details-headings">CSS3 Modules</h3>
				<p>CSS stands for 'Cascading Style Sheets' and is the language of design for web pages, giving site designers the ability to suggest how their site documents are styled. CSS is interpreted by all graphical web browsers regardless of device or operating system. It allows a logical division between the structure of a web page, (handled by the HTML) and the way it should look. CSS can be used to tailor the appearance of a web page specific to a particular device or screen size. In short, CSS is the language used for implementing front-end web design.</p>
				<ul id="MainMenu">
					<li>
						<a href="javascript:void(0)" class="lg">Module 1: Introduction to CSS 1.0 and 2.0<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Basics</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> CSS Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Syntax, CSS Id &amp; Class</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS Styling, Styling, Backgrounds</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Styling Text, Styling Fonts</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> Styling Links, Styling Lists</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Styling Tables</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> CSS Border </span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 2: Introduction to CSS3<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Introduction</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>CSS3 Modules</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Selectors</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Box Models</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Backgrounds &amp; Borders</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Text &amp; Effects</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>2D/3D Transformations</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Animations</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Multiple Column Element</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>User Interface</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 3: CSS3 Borders<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>border-radius</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>box-shadow</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>border-image</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 4: CSS3 Backgrounds<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>background-size</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>background-image</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>background-color</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>background-repeat</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>background-position</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 5: CSS3 Text Effects<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>text-shadow</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>word-wrap</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 6: CSS3 Fonts<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>@font-face rule</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>font-stretch</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>font-weight</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 7: CSS3 2D Transforms<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How does it work?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Browser Support</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> 2D Transform</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> translate()</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> rotate()</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> scale()</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> skew()</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> matrix()</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 8: CSS3 3D Transforms<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>rotateX()</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>rotateY()</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 9: CSS3 Transitions<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>How does it work?</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>transition-property</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>transition-duration</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>transition-delay</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 10: CSS3 Animations<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span> CSS3 @keyframes Rule</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Browser Support</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>Animation, animation-duration</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 11: CSS3 Multiple Columns<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>column-count</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>column-gap</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>column-rule</span></a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0)" class="lg">Module 12: CSS3 User Interface<i class="wkl-add list-toggle-icon"></i></a>
						<ul class="dropdown-list">
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>resize</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>box-sizing</span></a></li>
							<li><a href="javascript:void(0)"><span class="wkl-paper"></span><span>outline-offset</span></a></li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="course-details-learn course-details-block col-xs-12">
				<h3 class="course-details-headings">What will you are going to Learn?</h3>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Real-world skills to build real-world websites: professional, beautiful and truly responsive websites</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>A huge project that will teach you everything you need to know to get started with HTML5 and CSS3</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>The proven 7 real-world steps from complete scratch to a fully functional and optimized website</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Simple-to-use web design guidelines and tips to make your website stand out from the crowd</p>
						</div>
					</div>
				</div>
				<div class="row what-to-learn-row">
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create static HTML and CSS portfolio 
							sites and landing pages.</p>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 what-to-learn-div">
						<div class="col-xs-1 check-icon1 no-padding">
							<span class="wkl-check-mark-button"></span>
						</div>
						<div class="col-xs-11">
							<p>Create User-Friendly websites with attractive designs.</p>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<div class="col-md-3 col-sm-4 padding-left0">
			<div class="course-details-fix-div course-dtls-side-div col-xs-12" id="fix-div">
				<!-- icon-circle -->
                <div class="course-circle1">
               		<span class="wkl-htmlncss2 course-icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
                </div>
                <P 	class="xlg">HTML5 & CSS3</P>
                <p class="text-center">
					<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
				</p>
				<p class="text-center">
					<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
				</p>
				<p class="certificate">
					<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
					</span>  
				</p>
			</div>
		</div>
		<div class="course-dtls-side-div1 course-dtls-side-div col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
			<!-- icon-circle -->
            <div class="course-circle1">
           		<span class="wkl-htmlncss2 course-icon2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
            </div>
            <P 	class="xlg">HTML5 & CSS3</P>
            <p class="text-center">
				<a href="<?php echo get_site_url(); ?>/registration" class="btn register2-btn">Register</a>
			</p>
			<p class="text-center">
				<a href="<?php bloginfo('template_directory'); ?>/pdf/WebGurukul-WebDesigningCourse.pdf" target="_blank" class="btn download2-btn">Download Brochure </a>
			</p>
			<p class="certificate">
				<span class="wkl-diploma certificate-icon"></span> <span class="certificate-text">Certificate of Completion
				</span>  
			</p>
		</div>
	</div>
</section>
<!--end course details section-1 -->
								
<!-- other course slider -->
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
<div class="other-course-section padding-top-bot" id="mixedSlider">
	<h2 class="text-center">Other Courses</h2>
    <div class="MS-content">
    	<?php $i=0; while ( have_posts() ) : the_post(); ?>
        <div class="item">
			<div class="course-module-div">
			    <div class="course-circle">
                	<img src="<?php the_post_thumbnail_url(); ?>">
                </div>         
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>
                <a href="<?php echo get_page_link(); ?>" class="btn border-btn1 courses-border-btn1">More Details</a>
	        </div>
		</div>
		<?php $i++; endwhile; ?>                        
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
</div>

<!--other-course-section  below768 -->
<section class="other-course-section1 padding-top-bot">
 <?php  $args = array('post_type'=> 'courses_slider');
    query_posts( $args );
    ?>
	<div class="container">
	<div class="course-list-div course-dtls-side-div col-xs-12">
			<h2 class="course-list-heading">Other Courses</h2>
			<ul>
				<?php $i=0; while ( have_posts() ) : the_post(); ?>
					<li><a href="<?php echo get_page_link(); ?>"><?php the_title(); ?></a></li>
				<?php $i++; endwhile; ?>       
			</ul>
		</div>
	</div>
</section>

<?php get_footer(); ?>